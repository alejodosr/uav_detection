project(uav_detection)
cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
add_definitions(-DOPENCV)
#set(OpenCV_LIBS opencv_core opencv_imgproc opencv_calib3d opencv_video opencv_features2d opencv_ml opencv_highgui opencv_objdetect   )

SET(CMAKE_CXX_FLAGS "-std=c++11 -O3 -pthread")

set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS ON)
option(GPU         		  "use GPU"                 OFF)
option(OPENCV      		  "use OPENCV"              ON)
option(OPENMP      		  "use OPENMP"              OFF)
option(CUDNN       		  "use CUDNN"               OFF)
option(MULTI_THREAD               "use MULTI_THREAD"        ON)
option(ECO                        "use ECO Tracker"         OFF)

option(USE_ROS "USE ROS LIBRARY" ON)
option(USE_TORCH "USE TORCH LIBRARY" OFF)

#SET(OpenCV_DIR /media/alejandro/DATA_SSD/temporary/installation/OpenCV-master/lib/cmake/opencv4)


if(NOT CMAKE_BUILD_TYPE)
        set(CMAKE_BUILD_TYPE RELEASE)
endif()
message(STATUS "compile type：" ${CMAKE_BUILD_TYPE})

set(SIMD 2)
if(SIMD EQUAL 1)
        message(STATUS "use sse-Intel to compile")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse4")
        add_definitions(-DUSE_SIMD)
elseif(SIMD EQUAL 2)
        message(STATUS "use neon-TX2 to compile")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ffast-math -flto -march=armv8-a+crypto -mcpu=cortex-a57+crypto")
        add_definitions(-DUSE_SIMD)
        add_definitions(-DUSE_NENOS)
elseif(SIMD EQUAL 0)
        message(STATUS "no-simd to compile")
endif()

if(MULTI_THREAD)
        add_definitions(-DUSE_MULTI_THREAD)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
endif()

#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OPTIMAZE_FLAG} -Wall -Wno-unknown-pragmas -Wfatal-errors -fPIC")
#set(CMAKE_C_FLAGS_DEBUG "-O0 -g")
#set(CMAKE_C_FLAGS_RELEASE "-O3")

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lm -pthread -Wall -lstdc++ -std=c++0x")
#set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
#set(CMAKE_CXX_FLAGS_RELEASE "-O3")

#list(APPEND CMAKE_PREFIX_PATH "lib/torch")

#adding ros dependencies
if(USE_ROS)
        find_package(catkin REQUIRED
    COMPONENTS roscpp std_msgs cv_bridge image_transport sensor_msgs message_generation tf_conversions)
endif()

if(USE_TORCH)
find_package(Torch REQUIRED)    # Now compatible with version Pytorch Commit #83221655a8237ca80f9673dad06a98d34c43e546 (remember git submodule update --init)
endif()
find_package(OpenCV REQUIRED)
find_package(Eigen3)
if(NOT EIGEN3_FOUND)
  # Fallback to cmake_modules
  find_package(cmake_modules REQUIRED)
  find_package(Eigen REQUIRED)
  set(EIGEN3_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
  set(EIGEN3_LIBRARIES ${EIGEN_LIBRARIES})  # Not strictly necessary as Eigen is head only
  # Possibly map additional variables to the EIGEN3_ prefix.
  message(WARN "Using Eigen2!")
else()
  set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
endif()

if(USE_ROS)
#add message files for detection
add_message_files(
    FILES
    ObjectInfo.msg
    DetectedObjects.msg
    )

generate_messages(
    DEPENDENCIES
    std_msgs
    sensor_msgs
    )
endif()

if(ECO)
# ECO tracker
file(GLOB eco_srcs
        include/eco/*.hpp
        include/eco/*.h
        include/eco/eco.cc
        include/eco/feature_extractor.cc
        include/eco/feature_operator.cc
        include/eco/ffttools.cc
        include/eco/fhog.cc
        include/eco/interpolator.cc
        include/eco/metrics.cc
        include/eco/optimize_scores.cc
        include/eco/regularization_filter.cc
        include/eco/sample_update.cc
        include/eco/scale_filter.cc
        include/eco/training.cc)
endif()
if(USE_ROS)
catkin_package(
    INCLUDE_DIRS include
    CATKIN_DEPENDS roscpp std_msgs cv_bridge image_transport sensor_msgs message_runtime tf_conversions)
endif()

# Include directories
include_directories(${OpenCV_INCLUDE_DIRS} include)

if(USE_ROS)
	#ros include
	include_directories(${catkin_INCLUDE_DIRS})
endif()

if(ECO)
if(SIMD GREATER 0)
    set(eco_srcs ${eco_srcs} ${CMAKE_CURRENT_LIST_DIR}/include/eco/gradient.cpp)
endif()
endif()

if(USE_TORCH)
add_executable(test_pretrained_model src/test_pretrained_model.cpp)
target_link_libraries(test_pretrained_model "${TORCH_LIBRARIES}" ${OpenCV_LIBS})
set_property(TARGET test_pretrained_model PROPERTY CXX_STANDARD 11)

add_executable(test_retinanet_single_image src/test_retinanet_single_image.cpp)
target_link_libraries(test_retinanet_single_image "${TORCH_LIBRARIES}" ${OpenCV_LIBS})
set_property(TARGET test_retinanet_single_image PROPERTY CXX_STANDARD 11)

add_executable(test_retinanet_multiple_images src/test_retinanet_multiple_images.cpp)
target_link_libraries(test_retinanet_multiple_images "${TORCH_LIBRARIES}" ${OpenCV_LIBS})
set_property(TARGET test_retinanet_multiple_images PROPERTY CXX_STANDARD 11)

#add_library(ecotracker SHARED ${eco_srcs})
add_executable(test_retinanet_tracker_simple src/test_retinanet_tracker_simple.cpp ${eco_srcs})
target_link_libraries(test_retinanet_tracker_simple "${TORCH_LIBRARIES}" ${OpenCV_LIBS})
set_property(TARGET test_retinanet_tracker_simple PROPERTY CXX_STANDARD 11)
endif()

#ros node
if(USE_ROS)
    if(USE_TORCH)
        add_executable(test_retinanet_ros_image src/test_retinanet_ros_image.cpp)
        target_link_libraries(test_retinanet_ros_image ${OpenCV_LIBS})
        #target_link_libraries(test_retinanet_ros_image  ${catkin_EXPORTED_TARGETS})
        target_link_libraries(test_retinanet_ros_image  ${catkin_LIBRARIES})
        target_link_libraries(test_retinanet_ros_image ${TORCH_LIBRARIES})
        #set_property(TARGET test_retinanet_ros_image PROPERTY CXX_STANDARD 11)

        add_executable(test_retinanet_tracker_simple_ros src/test_retinanet_tracker_simple_ros.cpp ${eco_srcs})
        target_link_libraries(test_retinanet_tracker_simple_ros ${OpenCV_LIBS})
        #target_link_libraries(test_retinanet_tracker_simple_ros  ${catkin_EXPORTED_TARGETS})
        target_link_libraries(test_retinanet_tracker_simple_ros  ${catkin_LIBRARIES})
        target_link_libraries(test_retinanet_tracker_simple_ros ${TORCH_LIBRARIES})
        #set_property(TARGET test_retinanet_tracker_simple_ros PROPERTY CXX_STANDARD 11)
    else()
#        add_executable(test_only_tracker_ros src/test_only_tracker_ros.cpp ${eco_srcs})
#        target_link_libraries(test_only_tracker_ros ${OpenCV_LIBS})
#        target_link_libraries(test_only_tracker_ros  ${catkin_LIBRARIES})
#        add_dependencies(test_only_tracker_ros uav_detection_generate_messages_cpp)

        add_subdirectory(darknet)

        if(ECO)
            add_executable(test_detector_eco_tracker_pnp src/test_detector_eco_tracker_pnp.cpp ${eco_srcs})
            target_link_libraries(test_detector_eco_tracker_pnp ${OpenCV_LIBS})
            target_link_libraries(test_detector_eco_tracker_pnp  ${catkin_LIBRARIES})
            target_link_libraries(test_detector_eco_tracker_pnp dark)
            add_dependencies(test_detector_eco_tracker_pnp uav_detection_generate_messages_cpp)
            set_property(TARGET test_detector_eco_tracker_pnp PROPERTY CXX_STANDARD 11)
        endif()

        add_executable(test_detector_opencv_tracker_pnp src/test_detector_opencv_tracker_pnp.cpp)
        target_link_libraries(test_detector_opencv_tracker_pnp ${OpenCV_LIBS})
        target_link_libraries(test_detector_opencv_tracker_pnp  ${catkin_LIBRARIES})
        target_link_libraries(test_detector_opencv_tracker_pnp dark)
        add_dependencies(test_detector_opencv_tracker_pnp uav_detection_generate_messages_cpp)
        set_property(TARGET test_detector_opencv_tracker_pnp PROPERTY CXX_STANDARD 11)

    endif()
endif()

