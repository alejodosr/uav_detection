#ifndef RETINANET_H
#define RETINANET_H

#include <torch/script.h> // One-stop header.
#include "anchors.h"
#include "bbox_transform.h"
#include "clip_boxes.h"
#include "utils.h"

#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

class RetinanetModel  : torch::nn::Module{
    // Define the size of the input square tensor
    const int SQUARE_SIZE_ = 512;
    // Define number of classes
    const int NUM_CLASSES_ = 1;
    // Define the threshold to be considered from the class
    const float THRESHOLD_ = 0.5;
    // Center crop image for small objects percentage
    const float CROP_PERCENTAGE_ = 0.0;
    // Labels of the application
    const std::vector<std::string> LABELS_{ "drone", "bird", "car" };
    // Number of milliseconds to wait in imshow if required
    const int WAIT_SHOW_TIME_ = 1;

public:
    RetinanetModel(const std::string model_path, const std::string device = "gpu"){
        std::cout << "###### MODEL RETINANET ALLOCATION ######" << std::endl;

        // Deserialize the ScriptModule from a file using torch::jit::load().
        module_ = torch::jit::load(model_path);

        // Store device
        device_ = device;

        assert(module_ != nullptr);
        std::cout << "[OK] Module deserialized correctly\n";

        // Retinanet allocation
        if (device == "gpu"){
            auto start = std::chrono::high_resolution_clock::now();
            //  Move to CUDA
            module_->to(at::kCUDA);
            // After function call
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            // To get the value of duration use the count()
            // member function on the duration object
            std::cout << "Time to move module to GPU memory: " << static_cast<float>(duration.count()) / 1E6 << std::endl;
        }

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        auto start = std::chrono::high_resolution_clock::now();

        // Anchors allocation
        if (device == "gpu"){
            anchors_module_->to(at::kCUDA);
        }

        // After function call
        auto stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model Anchors allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        // Generate transforms
        if (device == "gpu"){
            bbox_module_->to(at::kCUDA);
        }
        else{
            bbox_module_->move2CPU();
        }
        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model Bbox allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        // Generate transforms
        if (device == "gpu"){
            // Move model to GPU
            clip_boxes_module_->to(at::kCUDA);
        }

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        // Pre-allocate tensor
        image_batch_normalized_tensor_ = torch::ones({SQUARE_SIZE_, SQUARE_SIZE_, 3}, torch::TensorOptions().dtype(torch::kFloat32)).transpose(0, 2).transpose(1, 2).unsqueeze(0);
        if (device == "gpu"){
            image_batch_normalized_tensor_.to(at::kCUDA);
        }
        // Inference on anchors model
        anchors_ = anchors_module_->forward({image_batch_normalized_tensor_});

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float anchors_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model Anchors processing time (cpu): " << anchors_time << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        if (device == "gpu"){
            anchors_ = anchors_.to(at::kCUDA);
        }
        //      regression = regression.detach().to(at::kCPU);

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Moving anchors from CPU to GPU: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model ClipBoxes allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;
    }
    ~RetinanetModel(){

    }

private:
    std::shared_ptr<torch::jit::script::Module> module_;
    std::shared_ptr<Anchors> anchors_module_ = std::make_shared<Anchors>();
    std::shared_ptr<BBoxTransform> bbox_module_ = std::make_shared<BBoxTransform>();
    std::shared_ptr<ClipBoxes> clip_boxes_module_ = std::make_shared<ClipBoxes>();
    at::Tensor image_batch_normalized_tensor_, anchors_;
    std::string device_;
    cv::Mat image_;
    std::vector<int> out_bbs_, out_classification_;
    std::vector<float> out_scores_;

    void showImage_(cv::Mat image)
    {
        cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
        cv::imshow("Display window", image );
        cv::waitKey(0);
    }

    std::tuple<float,float,float> resizeAndPad_(cv::Mat &src, cv::Mat &dst, int min_side = 512, int max_side = 512, float crop_percentage = 0){
        // Copy image
        cv::Mat tmp;
        src.copyTo(tmp);

        // Crop image
        float offset_x = 0;
        float offset_y = 0;
        if (crop_percentage != 0){
          offset_x = float(tmp.cols) * crop_percentage;
          offset_y = float(tmp.rows) * crop_percentage;
          tmp = tmp(cv::Rect(int(float(tmp.cols) * crop_percentage), int(float(tmp.rows) * crop_percentage),
                             int(float(tmp.cols) * (1 - (2 * crop_percentage))), int(float(tmp.rows) * (1 - (2 *crop_percentage)))));
        }

        // Check scale
        int shortest_side = std::min(tmp.rows, tmp.cols);

        // Compute scale
        float scale = float(min_side) / float(shortest_side);

        if(scale > 0.0){
            // Compute largest side
            int largest_side = std::max(tmp.rows, tmp.cols);
            if (float(largest_side) * scale > float(max_side)){
                scale = float(max_side) / float(largest_side);
            }

            std::cout << "Scale of the transformation: " << scale << std::endl;

            cv::resize(tmp, tmp, cv::Size(), scale, scale);
        }

        // Pad image if required
        int pad_w = max_side - tmp.rows%max_side;
        int pad_h = max_side - tmp.cols%max_side;

        if (tmp.rows%max_side == 0){
            pad_w = 0;
        }
        if (tmp.cols%max_side == 0){
            pad_h = 0;
        }

        dst = cv::Mat::zeros(cv::Size(tmp.rows + pad_w, tmp.cols + pad_h), CV_8UC3);

        cv::Rect roi(0,0, tmp.cols, tmp.rows);

        // Copy to padded image
        tmp.copyTo(dst(roi));

//        if (crop_percentage != 0){
//          shortest_side = std::min(original_tmp.rows, original_tmp.cols);

//          scale = float(min_side) / float(shortest_side);

//          if(scale > 0.0){
//            // Compute largest side
//            int largest_side = std::max(original_tmp.rows, original_tmp.cols);
//            if (float(largest_side) * scale > float(max_side)){
//              scale = float(max_side) / float(largest_side);
//            }

//            std::cout << "Scale of the transformation: " << scale << std::endl;
//          }
//        }

        return std::make_tuple(scale, offset_x, offset_y);;
    }

public:

    std::tuple<float,float,float> preprocess_image_(cv::Mat src){
        // Print info
        std::cout << std::endl << "###### MODEL RETINANET INFERENCE ######" << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        auto start = std::chrono::high_resolution_clock::now();

        cv::cvtColor(src, src, cv::COLOR_BGR2RGB);

        //      cv::resize(src, image, cv::Size(SQUARE_SIZE,SQUARE_SIZE));
        std::tuple<float, float, float> tuple = resizeAndPad_(src, image_, SQUARE_SIZE_, SQUARE_SIZE_, CROP_PERCENTAGE_);

        //      showImage_(image);

        // From image to tensor
        std::vector<int64_t> sizes = {image_.rows, image_.cols, 3};
        at::TensorOptions options(at::ScalarType::Byte);
        auto output_tensor_ = torch::from_blob(image_.data, at::IntList(sizes), options);

        auto image_tensor_ = output_tensor_.toType(at::kFloat) / 255;
        if (device_ == "gpu")
            image_tensor_ = image_tensor_.to(at::kCUDA);

        // Detach tensor
        //        image_tensor_.detach_();

        // Reshape image into 1 x 3 x height x width
        auto image_batch_tensor_ = image_tensor_.transpose(0, 2).transpose(1, 2).unsqueeze(0);

        // Normalize batch
        auto mean_value_ = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
        if (device_ == "gpu")
            mean_value_ = mean_value_.to(at::kCUDA);

        mean_value_[0][0][0][0] = 0.485f;
        mean_value_[0][1][0][0] = 0.456f;
        mean_value_[0][2][0][0] = 0.406f;

        // Broadcast the value
        auto mean_value_broadcasted_ = mean_value_.expand(image_batch_tensor_.sizes());

        auto std_value_ = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
        if (device_ == "gpu")
            std_value_ = std_value_.to(at::kCUDA);

        std_value_[0][0][0][0] = 0.229f;
        std_value_[0][1][0][0] = 0.224f;
        std_value_[0][2][0][0] = 0.225f;

        auto std_value_broadcasted_ = std_value_.expand(image_batch_tensor_.sizes());

        image_batch_normalized_tensor_ = (image_batch_tensor_ - mean_value_broadcasted_) / std_value_broadcasted_;

        // Detach
        image_batch_tensor_.detach();
        mean_value_broadcasted_.detach();
        std_value_broadcasted_.detach();

        // After function call
        auto stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float retinanet_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model image preprocess processing time (" << device_ << "): " << retinanet_time << std::endl;

        return tuple;
    }

    std::tuple<std::vector<int>, std::vector<float>, std::vector<int>> forward_(bool show_image = false){
        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        auto start = std::chrono::high_resolution_clock::now();

        // Execute the model and turn its output into a tensor.
        auto output_ = module_->forward({image_batch_normalized_tensor_}).toTensor();

        // After function call
        auto stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float retinanet_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model Retinanet processing time (" << device_ << "): " << retinanet_time << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        // Get regression and classifications
        auto regression_ = output_.slice(2, 0, 4);
        auto classification_ = output_.slice(2, 4, 5 + NUM_CLASSES_);

        //      regression = regression.to(at::kCPU);

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        //      std::cout << anchors.slice(0, 0, 3).slice(1, 0, 3) << std::endl;
        //      std::cout << regression.slice(0, 0, 3).slice(1, 0, 3) << std::endl;

        // Inference on anchors model
        auto transformed_anchors_ = bbox_module_->forward({anchors_}, {regression_});

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float bbox_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model Bbox processing time (cpu): " << bbox_time << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        //      transformed_anchors = transformed_anchors.to(at::kCPU);

        // Inference on anchors model
        transformed_anchors_ = clip_boxes_module_->forward({transformed_anchors_}, {image_batch_normalized_tensor_});

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float clip_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "Model ClipBoxes processing time (cpu): " << clip_time << std::endl;

        // Use auto keyword to avoid typing long
        // type definitions to get the timepoint
        // at this instant use function now()
        start = std::chrono::high_resolution_clock::now();

        auto scores_tuple_ = torch::max(classification_, 2, true);
        auto scores_ = std::get<0>(scores_tuple_);
        auto indices_ = std::get<1>(scores_tuple_);

        auto keep_count_ = non_max_supression(transformed_anchors_.squeeze(0).detach()/*.cpu()*/, scores_.squeeze(0).detach()/*.cpu()*/, device_);

        auto keep_ = std::get<0>(keep_count_);
        auto count_ = std::get<1>(keep_count_);

        // After function call
        stop = std::chrono::high_resolution_clock::now();

        // Subtract stop and start timepoints and
        // cast it to required unit. Predefined units
        // are nanoseconds, microseconds, milliseconds,
        // seconds, minutes, hours. Use duration_cast()
        // function.
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        float nms_time = static_cast<float>(duration.count()) / 1E6;

        // To get the value of duration use the count()
        // member function on the duration object
        std::cout << "NMS processing time (cpu): " << nms_time << std::endl;

        std::cout << "TOTAL PROCESSING TIME: " << retinanet_time + bbox_time + clip_time + nms_time << std::endl;


        bool result = true;
        // Clear bounding boxes
        out_bbs_.clear();
        out_scores_.clear();
        out_classification_.clear();

        //
        cv::cvtColor(image_, image_, cv::COLOR_RGB2BGR);

        for(int i = 0; i < count_; i++){
            int index = keep_.slice(0, i, i +1).squeeze(1).item<int>();
            float result = scores_.slice(1, index, index + 1).squeeze(0).squeeze(1).item<float>();
            if(result > THRESHOLD_){
                int x1 = transformed_anchors_.slice(1, index, index + 1).slice(2, 0, 1).squeeze(0).item<int>();
                int y1 = transformed_anchors_.slice(1, index, index + 1).slice(2, 1, 2).squeeze(0).item<int>();
                int x2 = transformed_anchors_.slice(1, index, index + 1).slice(2, 2, 3).squeeze(0).item<int>();
                int y2 = transformed_anchors_.slice(1, index, index + 1).slice(2, 3, 4).squeeze(0).item<int>();
                out_bbs_.push_back(x1);
                out_bbs_.push_back(y1);
                out_bbs_.push_back(x2);
                out_bbs_.push_back(y2);
                out_scores_.push_back(result);
                cv::rectangle(image_, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0), 2);
                auto values_indices = torch::max(classification_.slice(1, index, index + 1), 2, true);
                auto indices = std::get<1>(values_indices);
                out_classification_.push_back(indices.squeeze(0).squeeze(0).item<int>());
                if(show_image){
                    std::string label = LABELS_[indices.squeeze(0).squeeze(0).item<int>()];
                    std::ostringstream ss;
                    ss << label << " " << result;
                    cv::putText(image_, ss.str(), cv::Point(x1, y1 - 7), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 2.0);
                }
            }
        }

        if(show_image){
            cv::namedWindow( "Retinanet result", cv::WINDOW_AUTOSIZE );// Create a window for display.
            cv::imshow( "Retinanet result", image_ );
            char key = (char) cv::waitKey(WAIT_SHOW_TIME_);   // explicit cast
            if (key == 27) result = false;                // break if `esc' key was pressed.
            if (!result){
                out_bbs_.clear();
                out_bbs_.push_back(-1);
                out_scores_.clear();
                out_classification_.clear();
                return std::make_tuple(out_bbs_, out_scores_, out_classification_);
            }
        }



        // Return bounding boxes
        return std::make_tuple(out_bbs_, out_scores_, out_classification_);
    }


};

#endif
