#ifndef ANCHORS_H
#define ANCHORS_H

#include <torch/torch.h>
// Eigen
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <ATen/ATen.h>
#include <math.h>
#include "utils.h"

// Anchors module
struct Anchors : torch::nn::Module {
    Anchors() {
//        // Pyramid levels
          pyramid_levels_.push_back(3);
          pyramid_levels_.push_back(4);
          pyramid_levels_.push_back(5);
          pyramid_levels_.push_back(6);
          pyramid_levels_.push_back(7);

//        // Strides
          strides_.push_back(8);
          strides_.push_back(16);
          strides_.push_back(32);
          strides_.push_back(64);
          strides_.push_back(128);

//        // Sizes
          sizes_.push_back(32);
          sizes_.push_back(64);
          sizes_.push_back(128);
          sizes_.push_back(256);
          sizes_.push_back(512);

//        // Ratios
          ratios_.push_back(0.5);
          ratios_.push_back(1.0);
          ratios_.push_back(2.0);

//        // Scales
          scales_.push_back(1.0);
          scales_.push_back(1.25992);
          scales_.push_back(1.587401);
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor x) {
        // Read input shape
        auto x_shape = x.sizes();

        // Compute pyramid levels
        Eigen::VectorXf image_shape(2);
        image_shape << x_shape[2], x_shape[3];

        // Compute shapes of the pyramids
        Eigen::MatrixXf image_shapes;
        for (int i=0; i<pyramid_levels_.size(); i++){
            Eigen::RowVectorXf tmp(2);
            tmp << float(int((image_shape(0) + pow(2.0, pyramid_levels_[i])  - 1) / pow(2.0, pyramid_levels_[i]))),
                    float(int((image_shape(1) + pow(2.0, pyramid_levels_[i]) - 1) / pow(2.0, pyramid_levels_[i])));
            image_shapes.conservativeResize(image_shapes.rows() + 1, 2);
            image_shapes.row(image_shapes.rows() - 1) = tmp;
        }

        // Compute anchors over all pyramids
        Eigen::MatrixXf all_anchors = Eigen::MatrixXf::Zero(0,4);

        // Generate anchors
        for (int i = 0; i < pyramid_levels_.size(); i++){
            Eigen::MatrixXf anchors = generate_anchors(int(sizes_[i]), from_std_vector2eigen(ratios_), from_std_vector2eigen(scales_));
            Eigen::MatrixXf shifted_anchors = shift(image_shapes.row(i), int(strides_[i]), anchors);
            Eigen::MatrixXf prev_all_anchors = all_anchors;
            all_anchors.conservativeResize(all_anchors.rows() + shifted_anchors.rows(), all_anchors.cols());
//            // eigen uses provided dimensions in declaration to determine
//            // concatenation direction
            all_anchors << prev_all_anchors, shifted_anchors;
        }


//        // Convert to torch tensor
        auto all_anchors_tensor = mat2tensor_extend_dim(all_anchors);

        return all_anchors_tensor;

    }

    Eigen::MatrixXf generate_anchors(int size, Eigen::VectorXf ratios, Eigen::VectorXf scales){
        int num_anchors = ratios.rows() * scales.rows();

        // Initialize output anchors
        Eigen::MatrixXf anchors = Eigen::MatrixXf::Zero(num_anchors, 4);

        // Scale base size
        for (int i = 0; i < int(anchors.rows() / ratios.rows()); i++){
            anchors.block((i * 3), 2, ratios.rows(), 1) = size * scales;
            anchors.block((i * 3), 3, ratios.rows(), 1) = size * scales;
        }

        // Compute areas of anchors
        Eigen::VectorXf areas = anchors.col(2).cwiseProduct(anchors.col(3));

        // Correct for ratios
        Eigen::VectorXf ratios_extended(ratios.rows() * 3);
        ratios_extended << ratios(0), ratios(0), ratios(0), ratios(1), ratios(1), ratios(1), ratios(2), ratios(2), ratios(2);
        anchors.col(2) = (areas.cwiseProduct(ratios_extended.cwiseInverse())).cwiseSqrt();
        anchors.col(3) = anchors.col(2).cwiseProduct(ratios_extended);

        // Transform from (x_ctr, y_ctr, w, h) -> (x1, y1, x2, y2)
        anchors.col(0) -= anchors.col(2) * 0.5; // Check if implementation is correct
        anchors.col(1) -= anchors.col(3) * 0.5;
        anchors.col(2) -= anchors.col(2) * 0.5;
        anchors.col(3) -= anchors.col(3) * 0.5;

        return anchors;
    }

    Eigen::MatrixXf shift(Eigen::RowVectorXf image_shape, int stride, Eigen::MatrixXf anchors){
        Eigen::RowVectorXf tmp_half_x = Eigen::RowVectorXf::Ones(image_shape(1)) * 0.5;
        Eigen::RowVectorXf tmp_half_y = Eigen::RowVectorXf::Ones(image_shape(0)) * 0.5;
        Eigen::VectorXf shift_x = ((Eigen::RowVectorXf::LinSpaced(image_shape(1), 0, image_shape(1) - 1) + tmp_half_x) * stride).transpose(); // Size = ((hi-low)/step)+1
        Eigen::VectorXf shift_y = ((Eigen::RowVectorXf::LinSpaced(image_shape(0), 0, image_shape(0) - 1) + tmp_half_y) * stride).transpose(); // Size = ((hi-low)/step)+1

        Eigen::MatrixXf shift_x_mat, shift_y_mat;
        meshgrid(shift_x, shift_y, shift_x_mat, shift_y_mat);

        // Flatten
        shift_x_mat.resize(shift_x_mat.rows() * shift_x_mat.cols(), 1);
        shift_y_mat.resize(shift_y_mat.rows() * shift_y_mat.cols(), 1);

        Eigen::MatrixXf shifts(shift_x_mat.rows(), 4);
        shifts.col(0) = shift_y_mat;
        shifts.col(1) = shift_x_mat;
        shifts.col(2) = shift_y_mat;
        shifts.col(3) = shift_x_mat;
//        shifts.transpose();

        // add A anchors (1, A, 4) to
        // cell K shifts (K, 1, 4) to get
        // shift anchors (K, A, 4)
        // reshape to (K*A, 4) shifted anchors
        int A = anchors.rows();
        int K = shifts.rows();

        auto all_anchors_tensor = torch::ones({K, A, 4}, torch::TensorOptions().dtype(torch::kFloat32));
        auto aat = all_anchors_tensor.accessor<float, 3>(); // This takes into accout the stride

        for(int64_t k = 0; k < aat.size(0); k++){
            auto aat1 = aat[k];
            for(int64_t a = 0; a < aat1.size(0); a++){
                auto aat2 = aat1[a];
                for(int64_t i = 0; i < aat2.size(0); i++){
                    aat2[i] = anchors(a, i) + shifts(k, i);
                }
            }
        }

        all_anchors_tensor = all_anchors_tensor.reshape(torch::IntList({K * A, 4}));

        Eigen::MatrixXf all_anchors = tensor2mat(all_anchors_tensor);

        return all_anchors;
    }

    // Member variables
    std::vector<float> pyramid_levels_, strides_, sizes_, ratios_, scales_;
};
#endif /* ANCHORS_H */
