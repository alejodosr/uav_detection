#ifndef DETECTOR_H
#define DETECTOR_H

#include <iostream>
#include <memory>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include<list>
#include<vector>
// #include"YOLOv3SE.h"
#include <algorithm>
#define OPENCV
#include <yolo_v2_class.hpp>

using namespace std;

struct Detection{
  Detection() {}

    Detection(bbox_t b){
    class_name = "";
    x=b.x;
    y=b.y;
    w=b.w;
    h=b.h;
    c=b.obj_id;
    confidence= b.prob;
  }
  float x,y,w,h,confidence;
  int c;
  bool operator < (const Detection &d2) const{
    return this->confidence <= d2.confidence;
  }
  string class_name;
  bool has_pose() const {return pose;}
  bool has_detection() const {return detection;}
  float x_p, y_p, z_p, roll_p, pitch_p , yaw_p;
  bool pose=false, detection;

};

class DetectorWrapper {
public:
  DetectorWrapper() {} ;
  virtual void setScale(int scale) {this->scale = scale ; };
  virtual int getScale () {return this-> scale; };
  virtual std::list<Detection> detect(cv::Mat image) {};
  virtual std::vector<std::string> getClasses() const { return std::vector<std::string>();};
  virtual void setThreshold (float t){threshold = t;}
protected:
  float threshold=0.1f;
  int scale ;
};


class YOLO_detector : public DetectorWrapper{
public:
Detector detector;
  YOLO_detector ( std::string path) : detector((path+"checkpoint.cfg"), (path  + "checkpoint.weights") ){    }

  std::list<Detection> detect(cv::Mat image){
    std::list<Detection> detections;
    std::vector<bbox_t> result_vec;
    result_vec = detector.detect(image, this->threshold);
            for(auto it = result_vec.begin(); it<result_vec.end(); it ++){
              detections.push_back(Detection(*it) );
            }

//    std::sort(detections.begin() , detections.end());
    detections.sort();
    return detections;
  }
};

#endif
