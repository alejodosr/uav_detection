#ifndef COLORS_DETECTOR_H
#define COLORS_DETECTOR_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

cv::Rect DetectObjectInImage(const cv::Mat &I, const std::string object_color)
{
   // Variables declaration
   cv::Mat I_hsv;
   cv::cvtColor(I, I_hsv, CV_BGR2HSV);
   cv::Mat1b mask1_hsv, mask2_hsv;
   cv::Mat I_binarized;

   // Differentiate between colors
   if(object_color == "Red")
   {
       //Real
       cv::inRange(I_hsv, cv::Scalar(0, 100, 100), cv::Scalar(15, 255, 255), mask1_hsv);
       cv::inRange(I_hsv, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), mask2_hsv);

       // Simulation
//        cv::inRange(I_hsv, cv::Scalar(0, 70, 20), cv::Scalar(10, 255, 255), mask1_hsv);
//        cv::inRange(I_hsv, cv::Scalar(170, 70, 20), cv::Scalar(180, 255, 255), mask2_hsv);

       I_binarized = /*mask1_hsv |*/ mask2_hsv;
   }
   else if(object_color == "Blue")
   {
       //cv::inRange(I_hsv, cv::Scalar(90, 100, 50), cv::Scalar(140, 255, 255), I_mask_hsv); //Real
       cv::inRange(I_hsv, cv::Scalar(110, 70, 50), cv::Scalar(130, 255, 255), I_binarized); //Simulation
   }

   // Process contours
   std::vector<std::vector<cv::Point> > contours;
   cv::findContours(I_binarized.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

//   cv::imshow("Binarized camera", I_binarized);
//   cv::resize(I_binarized, I_binarized, cv::Size(I_binarized.cols/2, I_binarized.rows/2));
//   cv::waitKey(1);

   int counter_no_detection_ = 0;
   bool bool_no_detection_ = false;

   std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
   while(itc != contours.end())
   {
       cv::RotatedRect rot_box = cv::minAreaRect(cv::Mat(*itc));
       float rot_box_area = (float)rot_box.size.width * (float)rot_box.size.height;

       if(rot_box_area < 500)
           itc = contours.erase(itc);
       else
       {
           ++itc;
       }
       if(rot_box_area <100){
           counter_no_detection_ = counter_no_detection_ + 1;
           if (counter_no_detection_ > 50){
               bool_no_detection_ = true;
           }
       }else {
           counter_no_detection_ = 0;

       }
   }


   cv::RotatedRect rotated_rect_item;
   cv::Rect rect_item;

   if(contours.size())
   {
       rotated_rect_item = cv::minAreaRect(cv::Mat(contours[0]));
       rect_item = cv::boundingRect(cv::Mat(contours[0]));
   }

   return rect_item;

}

#endif
