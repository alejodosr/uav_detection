#ifndef CLIP_BOXES_H
#define CLIP_BOXES_H

#include <torch/torch.h>
// Eigen
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <ATen/ATen.h>
#include <math.h>
#include "utils.h"

// Anchors module
struct ClipBoxes : torch::nn::Module {
    ClipBoxes() {
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor boxes_tensor, torch::Tensor img_tensor) {
        // To tesor
        auto img_shape = img_tensor.sizes();
        int batch_size = img_shape[0];
        int num_channels = img_shape[1];
        int height = img_shape[2];
        int width = img_shape[3];

        boxes_tensor.slice(2, 0, 1) = torch::clamp_min(boxes_tensor.slice(2, 0, 1), 0);
        boxes_tensor.slice(2, 1, 2) = torch::clamp_min(boxes_tensor.slice(2, 1, 2), 0);

        boxes_tensor.slice(2, 2, 3) = torch::clamp_max(boxes_tensor.slice(2, 2, 3), width);
        boxes_tensor.slice(2, 3, 4) = torch::clamp_max(boxes_tensor.slice(2, 3, 4), height);

        return boxes_tensor;
    }
};
#endif
