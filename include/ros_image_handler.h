#ifndef ROS_IMAGE_HANDLER_H
#define ROS_IMAGE_HANDLER_H

#include <mutex>
#include <iostream>
#include <string>
#include <math.h>

#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

//ros messages
#include "sensor_msgs/Image.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Bool.h"
#include <cv_bridge/cv_bridge.h>
#include "image_transport/image_transport.h"
#include "uav_detection/DetectedObjects.h"
#include "uav_detection/ObjectInfo.h"

class ros_image_handle
{
public:

    //constructor
    ros_image_handle()
    {
    }

    //destructor
    ~ros_image_handle()
    {

    }

    //all the variable declarations go here
private:
    ros::Subscriber image_sub_;
    ros::Subscriber reset_sub_;
    ros::Publisher  bb_pub_;
    ros::Publisher  std_bb_pub_;
    ros::Publisher  image_pub_;

    std::mutex image_lock_;
    cv::Mat subscribed_image_;

    bool reset_flag_;

    std::string image_topic_;

    //all the functions go here
public:
    void read_params(std::string& model_path, std::string& device, int& show_image)
    {
        ros::param::get("~model_path", model_path);
        if(model_path.empty())
            model_path = "/home/hriday/workspace/ros/rovio_semantic_workspace/src/uav_detection/snapshots/pytorch_coco_retinanet_model.pt";
        std::cout << "model path " << model_path << std::endl;

        ros::param::get("~device", device);
        if(device.empty())
            device = "gpu";
        std::cout << "device " << device << std::endl;

        ros::param::get("~show_image", show_image);
        if(show_image != 1)
            show_image = 0;
        std::cout << "show_image " << show_image << std::endl;

    }

    void init_node(int argc, char **argv, std::string node_name)
    {
        reset_flag_ = false;

        ros::init(argc, argv, node_name);
        ros::NodeHandle n;

        ros::param::get("~image_topic", image_topic_);
        if(image_topic_.empty())
            image_topic_ = "/drone4/image_raw";
        std::cout << "image_topic " << image_topic_ << std::endl;

        this->activate_sub_pub(n);

    }

    bool spin()
    {
        if(ros::ok())
        {
            ros::spinOnce();
            return true;
        }
        else
            return false;
    }

    inline void get_cv_image(cv::Mat &image)
    {
        image_lock_.lock();
        image = subscribed_image_;
        image_lock_.unlock();
    }

    void publish_bb(std::vector<int> bbs, std::vector<float> scores,  std::vector<int> classes)
    {
        if (bbs.size() > 0){
            uav_detection::DetectedObjects bounding_box_vec;
            std_msgs::Float32MultiArray std_bbs_msg;

            int num_bbs = bbs.size()/4;

            for(int i=0; i < num_bbs; ++i)
            { //TODO: bounding box needs to be fixed
                uav_detection::ObjectInfo info;

                info.width  = bbs[i + 2] /*- bbs[i + 0]*/;
                info.height = bbs[i + 3] /*- bbs[i + 1]*/;
                info.tl_x   = bbs[i + 0];
                info.tl_y   = bbs[i + 1];
                if(classes[i] == 56)
                    info.type   = "chair";
                info.prob   = scores[i];
                bounding_box_vec.objects.push_back(info);

                // Fill std bbs
                std_bbs_msg.data.push_back(float(bbs[i + 0]));
                std_bbs_msg.data.push_back(float(bbs[i + 1]));
                std_bbs_msg.data.push_back(float(bbs[i + 2] /*- bbs[i + 0]*/));
                std_bbs_msg.data.push_back(float(bbs[i + 3] /*- bbs[i + 1]*/));
                std_bbs_msg.data.push_back(scores[i]);
                std_bbs_msg.data.push_back(float(classes[i]));

            }
            bb_pub_.publish(bounding_box_vec);
            std_bb_pub_.publish(std_bbs_msg);
        }
    }

    void publish_detected_image(cv::Mat image,  std::vector<int> bbs, std::vector<float> scores,  std::vector<int> classes)
    {
//        if (bbs.size() > 0){
//            int num_bbs = bbs.size()/4;

//            for(int i =0; i < num_bbs; ++i)
//            {
//                std::ostringstream ss;
//                ss << classes[i] << " " << scores[i];

//                cv::rectangle(image, cv::Point(bbs[i+0], bbs[i+1]), cv::Point(bbs[i+2], bbs[i+3]), cv::Scalar(0, 255, 0), 2);
//                cv::putText(image, ss.str(), cv::Point(bbs[i+0], bbs[i+1] - 7), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,255,255), 2.0);
//            }
//        }

        cv_bridge::CvImage img_bridge;
        std_msgs::Header image_header;
        image_header.stamp = ros::Time::now();
        sensor_msgs::Image ros_image;

        img_bridge = cv_bridge::CvImage(image_header, sensor_msgs::image_encodings::BGR8, image);
        img_bridge.toImageMsg(ros_image);
        image_pub_.publish(ros_image);
    }

    // Reads reset variable a restore reset
    inline bool get_reset()
    {
      bool temp = reset_flag_;
      reset_flag_ = false;
      return temp;
    }

    // Publish the tf of a object in camera frame
    void publish_tf(std::vector<double> rot, std::vector<double> trans){
      static tf::TransformBroadcaster broadcaster;
      tf::Transform transform;
      tf::Quaternion q;

      transform.setOrigin( tf::Vector3(trans[0], trans[1], trans[2]) );
      q.setRPY(rot[0], rot[1], rot[2]);
      transform.setRotation(q);
      broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "main_camera", "object"));
    }

private:

    void activate_sub_pub(ros::NodeHandle n)
    {
        //all subs
      std::cout << "Topic: " << image_topic_ << std::endl;
        image_sub_ = n.subscribe(image_topic_, 1, &ros_image_handle::imageCallback, this);
        reset_sub_ = n.subscribe("reset", 1, &ros_image_handle::resetCallback, this);

        //all pubs
        bb_pub_  = n.advertise<uav_detection::DetectedObjects>("/detector/bbs",1);
        std_bb_pub_  = n.advertise<std_msgs::Float32MultiArray>("/detector/std_bbs",1);
        image_pub_ = n.advertise<sensor_msgs::Image>("/detector/detected_image",1);
    }

    //image callback
    void resetCallback(const std_msgs::Bool &msg)
    {
        reset_flag_ = true;
    }


    //image callback
    void imageCallback(const sensor_msgs::Image &msg)
    {
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC3);
        } catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        cv::Mat cv_img;
        cv_ptr->image.copyTo(cv_img);
        cv::cvtColor(cv_img, cv_img, CV_RGB2BGR);
        this->set_cv_image(cv_img);
    }

    inline void set_cv_image(cv::Mat image)
    {
        image_lock_.lock();
        subscribed_image_ = image;
        image_lock_.unlock();
    }

};

#endif
