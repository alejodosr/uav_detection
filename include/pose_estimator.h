#ifndef POSE_ESTIMATOR_H
#define POSE_ESTIMATOR_H

#include <opencv2/opencv.hpp>

class PoseEstimator
{
public:

    //constructor
    PoseEstimator(cv::Mat cam_param, cv::Mat dist_coeffs)
    {
      cam_param.copyTo(camera_parameters_);
      dist_coeffs.copyTo(dist_coeffs_);
    }

    //destructor
    ~PoseEstimator()
    {}

private:
    std::vector<cv::Point2d> image_points_;
    std::vector<cv::Point3d> object_points_;
    cv::Mat camera_parameters_;
    cv::Mat dist_coeffs_;

public:

    void set2DImagePoints(std::vector<cv::Point2d> image_points){
      image_points_ = image_points;
    }

    void set3DObjectPoints(std::vector<cv::Point3d> object_points){
      object_points_ = object_points;
    }

    void estimatePose(cv::Mat &rot, cv::Mat &trans, char algorithm=0, bool use_ransac=true){
      if (algorithm == 0x1){ // CV_ITERATIVE
        if (use_ransac){
          cv::solvePnP(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans, false, cv::SOLVEPNP_ITERATIVE);
        }
        else{
          cv::solvePnPRansac(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans,
                                                                      false, 100, 8.0, 100, cv::noArray(), cv::SOLVEPNP_ITERATIVE);
        }
      }
      else if (algorithm == 0x2){ // CV_P3P
        // Should be exactcly 4 points
        if (image_points_.size() != 4 || object_points_.size() != 4){
          throw std::invalid_argument( "POSE ESTIMATOR: Error, image and object points should be exactly 4 for this algorithm" );
        }

        if (use_ransac){
          cv::solvePnP(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans, false, cv::SOLVEPNP_P3P);
        }
        else{
          cv::solvePnPRansac(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans,
                                                                      false, 100, 8.0, 100, cv::noArray(), cv::SOLVEPNP_P3P);
        }
      }
      else if (algorithm == 0x3){ // CV_EPNP
        if (use_ransac){
          cv::solvePnP(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans, false, cv::SOLVEPNP_EPNP);
        }
        else{
          cv::solvePnPRansac(object_points_, image_points_, camera_parameters_, dist_coeffs_, rot, trans,
                                                                      false, 100, 8.0, 100, cv::noArray(), cv::SOLVEPNP_EPNP);
        }
      }
      else{
        throw std::invalid_argument( "POSE ESTIMATOR: Error, algorithm for pose estimation not found" );
      }
    }
};

#endif
