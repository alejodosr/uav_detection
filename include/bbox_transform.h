#ifndef BBOX_TRANSFORM_H
#define BBOX_TRANSFORM_H

#include <torch/torch.h>
// Eigen
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <ATen/ATen.h>
#include <math.h>
#include "utils.h"

// Anchors module
struct BBoxTransform : torch::nn::Module {
    BBoxTransform() {
        // Mean value
        mean_.push_back(0.0);
        mean_.push_back(0.0);
        mean_.push_back(0.0);
        mean_.push_back(0.0);

        // Std value
        std_.push_back(0.1);
        std_.push_back(0.1);
        std_.push_back(0.2);
        std_.push_back(0.2);

        // To tensor
        mean_mat = from_std_vector2eigen(mean_);
        std_mat = from_std_vector2eigen(std_);
        mean_tensor = mat2tensor(mean_mat).to(at::kCUDA);
        std_tensor = mat2tensor(std_mat).to(at::kCUDA);

        mean_tensor = mean_tensor.transpose(1, 0);
        std_tensor = std_tensor.transpose(1, 0);
    }

    // Implement the Net's algorithm.
    torch::Tensor forward(torch::Tensor boxes_tensor, torch::Tensor deltas_tensor) {
        auto widths_tensor = boxes_tensor.slice(2, 2, 3) - boxes_tensor.slice(2, 0, 1);
        auto heights_tensor = boxes_tensor.slice(2, 3, 4) - boxes_tensor.slice(2, 1, 2);
        auto ctr_x_tensor = boxes_tensor.slice(2, 0, 1) + 0.5 * widths_tensor;
        auto ctr_y_tensor = boxes_tensor.slice(2, 1, 2) + 0.5 * heights_tensor;

        auto dx_tensor = deltas_tensor.slice(2, 0, 1) * std_tensor.slice(1, 0, 1) + mean_tensor.slice(1, 0, 1);
        auto dy_tensor = deltas_tensor.slice(2, 1, 2) * std_tensor.slice(1, 1, 2) + mean_tensor.slice(1, 1, 2);
        auto dw_tensor = deltas_tensor.slice(2, 2, 3) * std_tensor.slice(1, 2, 3) + mean_tensor.slice(1, 2, 3);
        auto dh_tensor = deltas_tensor.slice(2, 3, 4) * std_tensor.slice(1, 3, 4) + mean_tensor.slice(1, 3, 4);

        auto pred_ctr_x_tensor = ctr_x_tensor + dx_tensor * widths_tensor;
        auto pred_ctr_y_tensor = ctr_y_tensor + dy_tensor * heights_tensor;
        auto pred_w_tensor = torch::exp(dw_tensor) * widths_tensor;
        auto pred_h_tensor = torch::exp(dh_tensor) * heights_tensor;

        auto pred_boxes_x1_tensor = pred_ctr_x_tensor - 0.5 * pred_w_tensor;
        auto pred_boxes_y1_tensor = pred_ctr_y_tensor - 0.5 * pred_h_tensor;
        auto pred_boxes_x2_tensor = pred_ctr_x_tensor + 0.5 * pred_w_tensor;
        auto pred_boxes_y2_tensor = pred_ctr_y_tensor + 0.5 * pred_h_tensor;

        auto pred_boxes_tensor = torch::stack(torch::TensorList({pred_boxes_x1_tensor, pred_boxes_y1_tensor, pred_boxes_x2_tensor, pred_boxes_y2_tensor}), 2);

        return pred_boxes_tensor.squeeze(3);

    }

    void move2CPU(){
        mean_tensor = mean_tensor.to(at::kCPU);
        std_tensor = std_tensor.to(at::kCPU);
    }

    // Member variables
    std::vector<float> mean_, std_;

    // To tensor
    Eigen::VectorXf mean_mat;
    Eigen::VectorXf std_mat;
    torch::Tensor mean_tensor;
    torch::Tensor std_tensor;
};
#endif
