#ifndef UTILS_RETINANET_H
#define UTILS_RETINANET_H


#include <torch/torch.h>
// Eigen
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

Eigen::MatrixXf tensor2mat(torch::Tensor tensor){
    // Convert to mat
    int rows = int(tensor.sizes()[0]);
    int cols = int(tensor.sizes()[1]);

//    Eigen::MatrixXf mat(rows, cols);
//    auto p = tensor.accessor<float, 2>(); // This takes into accout the stride
//    for(int i=0; i < p.size(0); i++){
//        auto p1 = p[i];
//            std::cout << "I am here " << p.size(0) << std::endl;
//        for(int j=0; j < p1.size(0); j++){
//            mat(i,j) = p1[j];
//        }
//    }

    Eigen::MatrixXf mat(rows, cols);
//    auto p = tensor.accessor<float, 2>(); // This takes into accout the stride
    for(int i=0; i < rows; i++){
        for(int j=0; j < cols; j++){
            mat(i,j) = tensor.slice(0, i, i + 1).slice(1, j, j + 1).item<float>();
        }
    }

    return mat;
}

torch::Tensor mat2tensor(Eigen::MatrixXf mat, bool gpu = true){
    // Convert to tensor
    auto output_tensor = torch::ones({mat.rows(), mat.cols()}, torch::TensorOptions().dtype(torch::kFloat32));

    auto aat = output_tensor.accessor<float, 2>(); // This takes into accout the stride

    for(int64_t k = 0; k < aat.size(0); k++){
        auto aat1 = aat[k];
        for(int64_t a = 0; a < aat1.size(0); a++){
            aat1[a] = mat(k, a);
        }
    }
    if (gpu)
        return output_tensor.to(at::kCUDA);
    else
        return output_tensor;
}

torch::Tensor mat2tensor_extend_dim(Eigen::MatrixXf mat){
    // Convert to tensor
    auto output_tensor = torch::ones({mat.rows(), mat.cols()}, torch::TensorOptions().dtype(torch::kFloat32));
    auto aat = output_tensor.accessor<float, 2>(); // This takes into accout the stride

    for(int64_t k = 0; k < aat.size(0); k++){
        auto aat1 = aat[k];
        for(int64_t a = 0; a < aat1.size(0); a++){
            aat1[a] = mat(k, a);
        }
    }
    return output_tensor.unsqueeze(0);
}

Eigen::VectorXf from_std_vector2eigen(std::vector<float> v){
    Eigen::VectorXf eigen(v.size());
    for (int i = 0; i < v.size(); i++){
        eigen(i) = v[i];
    }
    return eigen;
}

//! Generates a mesh, just like Matlab's meshgrid
//  Template specialization for row vectors (Eigen::RowVectorXd)
//  in : x, y row vectors
//       X, Y matrices, used to save the mesh
template <typename Scalar>
void meshgrid(const Eigen::Matrix<Scalar, 1, -1>& x,
    const Eigen::Matrix<Scalar, 1, -1>& y,
    Eigen::Matrix<Scalar, -1, -1>& X,
    Eigen::Matrix<Scalar, -1, -1>& Y){
    Eigen::Matrix<Scalar, -1, 1> xt = x.transpose(), yt = y.transpose();
    meshgrid(xt, yt, X, Y);
}

//! Generates a mesh, just like Matlab's meshgrid
//  Template specialization for column vectors (Eigen::VectorXd)
//  in : x, y column vectors
//       X, Y matrices, used to save the mesh
template <typename Scalar>
void meshgrid(const Eigen::Matrix<Scalar, -1, 1>& x,
                const Eigen::Matrix<Scalar, -1, 1>& y,
                Eigen::Matrix<Scalar, -1, -1>& X,
                Eigen::Matrix<Scalar, -1, -1>& Y) {
    const long nx = x.size(), ny = y.size();
    X.resize(ny, nx);
    Y.resize(ny, nx);
    for (long i = 0; i < ny; ++i) {
        X.row(i) = x.transpose();
    }
    for (long j = 0; j < nx; ++j) {
        Y.col(j) = y;
    }
}

torch::Tensor selectLikeNumpy(torch::Tensor input, torch::Tensor idx){
//    // Output only if in idx is equal to one
//    auto inpt = input.accessor<float, 1>(); // This takes into accout the stride
//    auto it = idx.accessor<float, 1>(); // This takes into accout the stride
//    Eigen::MatrixXf mat;
//    for(int64_t k = 0; k < it.size(0); k++){
//        if(it[k] == 1.0){
//            mat.conservativeResize(mat.rows() + 1, 1);
//            mat(mat.rows() - 1, 0) = inpt[k];
//         }
//    }

    // Output only if in idx is equal to one
    auto inpt = input.accessor<float, 1>(); // This takes into accout the stride
    auto it = idx.accessor<float, 1>(); // This takes into accout the stride
    Eigen::MatrixXf mat;
    for(int64_t k = 0; k < it.size(0); k++){
        if(idx.slice(0, k, k + 1).item<float>() == 1.0){
            mat.conservativeResize(mat.rows() + 1, 1);
            mat(mat.rows() - 1, 0) = input.slice(0, k, k+1).item<float>();
         }
    }

    return mat2tensor(mat);
}

std::tuple<torch::Tensor, int> non_max_supression(torch::Tensor boxes, torch::Tensor scores, std::string device = "gpu", float overlap = 0.5, int top_k = 200){
//    """Apply non-maximum suppression at test time to avoid detecting too many
//    overlapping bounding boxes for a given object.
//    Args:
//        boxes: (tensor) The location preds for the img, Shape: [num_priors,4]. Implemented shape [num_priors, 4]
//        scores: (tensor) The class predscores for the img, Shape:[num_priors]. Implemented shape [num_priors, 1]
//        overlap: (float) The overlap thresh for suppressing unnecessary boxes.
//        top_k: (int) The Maximum number of box preds to consider.
//    Return:
//        The indices of the kept boxes with respect to num_priors.
//    """

    // Use auto keyword to avoid typing long
    // type definitions to get the timepoint
    // at this instant use function now()
    auto start = std::chrono::high_resolution_clock::now();

    auto keep = torch::ones({boxes.sizes()[0], 1}, torch::TensorOptions().dtype(torch::kFloat32));
    auto keep_mat = tensor2mat(keep);

    // After function call
    auto stop = std::chrono::high_resolution_clock::now();

    // Subtract stop and start timepoints and
    // cast it to required unit. Predefined units
    // are nanoseconds, microseconds, milliseconds,
    // seconds, minutes, hours. Use duration_cast()
    // function.
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    float nms_time = static_cast<float>(duration.count()) / 1E6;

    // To get the value of duration use the count()
    // member function on the duration object
    std::cout << "Intenal NMS processing time (cpu): " << nms_time << std::endl;

    if(boxes.numel() == 0){
        return std::make_tuple(keep, 0);
    }

    auto x1 = boxes.slice(1, 0, 1);
    auto y1 = boxes.slice(1, 1, 2);
    auto x2 = boxes.slice(1, 2, 3);
    auto y2 = boxes.slice(1, 3, 4);

    auto area = torch::mul((x2 - x1), (y2 - y1));

    auto v_idx = scores.sort(0);    // sort in ascending order
    auto v = std::get<0>(v_idx);
    auto idx = std::get<1>(v_idx);

    // I = I[v >= 0.01]
    idx = idx.slice(0, int(idx.sizes()[0]) - top_k, int(idx.sizes()[0])).toType(torch::kFloat32);

    auto idx_mat = tensor2mat(idx);

    auto xx1 = boxes;
    auto yy1 = boxes;
    auto xx2 = boxes;
    auto yy2 = boxes;
    auto w = boxes;
    auto h = boxes;

    // keep = torch.Tensor()
    int count = 0;
    while(idx_mat.rows() > 0){
        int i = int(idx_mat(idx_mat.rows() - 1, 0));  // index of current largest val

        // keep.append(i)
        keep_mat(count, 0) = float(i);

        count += 1;

        if (idx_mat.rows() == 1){
            break;
        }

        Eigen::MatrixXf idx_mat_tmp;
        idx_mat_tmp = idx_mat;
        idx_mat.conservativeResize(idx_mat.rows() - 1, 1);
        idx_mat = idx_mat_tmp.block(0, 0, idx_mat_tmp.rows() - 1, 1); // remove kept element from view

        //load bboxes of next highest
        if (device == "gpu")
            idx = mat2tensor(idx_mat, true);
        else
            idx = mat2tensor(idx_mat, false);

        xx1 = torch::index_select(x1, 0, idx.toType(at::kLong).squeeze(1));
        yy1 = torch::index_select(y1, 0, idx.toType(at::kLong).squeeze(1));
        xx2 = torch::index_select(x2, 0, idx.toType(at::kLong).squeeze(1));
        yy2 = torch::index_select(y2, 0, idx.toType(at::kLong).squeeze(1));

        // store element-wise max with next highest score
        xx1 = torch::clamp_min(xx1, x1.slice(0, i, i + 1).squeeze(1).item<float>());
        yy1 = torch::clamp_min(yy1, y1.slice(0, i, i + 1).squeeze(1).item<float>());
        xx2 = torch::clamp_max(xx2, x2.slice(0, i, i + 1).squeeze(1).item<float>());
        yy2 = torch::clamp_max(yy2, y2.slice(0, i, i + 1).squeeze(1).item<float>());

        w.resize_as_(xx2);
        h.resize_as_(yy2);

        w = xx2 - xx1;
        h = yy2 - yy1;

        // check sizes of xx1 and xx2.. after each iteration
        w = torch::clamp_min(w, 0);
        h = torch::clamp_min(h, 0);

        auto inter = w * h;

        // IoU = i / (area(a) + area(b) - i)
        auto rem_areas = torch::index_select(area, 0, idx.toType(at::kLong).squeeze(1));  // load remaining areas
//        auto union_tensor = mat2tensor((tensor2mat(rem_areas) - tensor2mat(inter)) + tensor2mat(area)(i, 1)); // Implement correctly
        auto union_tensor = rem_areas - inter + area.slice(0, i, i + 1); // Implement correctly
//        torch::Tensor union_tensor; // Dummy
        auto IoU = inter / union_tensor;  // store result in iou

        // keep only elements with an IoU <= overlap
        idx = selectLikeNumpy(idx.squeeze(1), IoU.le((overlap)).squeeze(1).toType(at::kFloat));
        idx_mat = tensor2mat(idx);  // Keep it up to date

    }
    if (device == "gpu")
        return std::make_tuple(mat2tensor(keep_mat, true), count);
    else
        return std::make_tuple(mat2tensor(keep_mat, false), count);
}

#endif

