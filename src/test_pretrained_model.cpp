#include <torch/script.h> // One-stop header.

#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

void showImage(cv::Mat image)
{
    cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow("Display window", image );
    cv::waitKey(0);
}

int main(int argc, const char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
    return -1;
  }

  // Deserialize the ScriptModule from a file using torch::jit::load().
  std::shared_ptr<torch::jit::script::Module> module = torch::jit::load(argv[1]);
  const std::string image_path = argv[2];
  const std::string device = argv[3];

  assert(module != nullptr);
  std::cout << "[OK] Module deserialized correctly\n";

  if (device == "gpu"){
      auto start = std::chrono::high_resolution_clock::now();
      //  Move to CUDA
      module->to(at::kCUDA);
      // After function call
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      // To get the value of duration use the count()
      // member function on the duration object
      std::cout << "Time to move module to GPU memory: " << static_cast<float>(duration.count()) / 1E6 << std::endl;
  }

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  auto start = std::chrono::high_resolution_clock::now();

  // Read image
  cv::Mat image;
  image = cv::imread(image_path, CV_LOAD_IMAGE_COLOR);
  cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
  cv::resize(image, image, cv::Size(224,224));
//  showImage(image);

  // From image to tensor
  std::vector<int64_t> sizes = {image.rows, image.cols, 3};
  at::TensorOptions options(at::ScalarType::Byte);
  at::Tensor output_tensor = torch::from_blob(image.data, at::IntList(sizes), options);

  auto image_tensor = output_tensor.toType(at::kFloat) / 255;
  if (device == "gpu")
    image_tensor = image_tensor.to(at::kCUDA);

  // Reshape image into 1 x 3 x height x width
  auto image_batch_tensor = image_tensor.transpose(0, 2).transpose(1, 2).unsqueeze(0);

  // Normalize batch
  auto mean_value = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
  if (device == "gpu")
      mean_value = mean_value.to(at::kCUDA);

  mean_value[0][0][0][0] = 0.485f;
  mean_value[0][1][0][0] = 0.456f;
  mean_value[0][2][0][0] = 0.406f;

  // Broadcast the value
  auto mean_value_broadcasted = mean_value.expand(image_batch_tensor.sizes());

  auto std_value = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
  if (device == "gpu")
      std_value = std_value.to(at::kCUDA);

  std_value[0][0][0][0] = 0.229f;
  std_value[0][1][0][0] = 0.224f;
  std_value[0][2][0][0] = 0.225f;

  auto std_value_broadcasted = std_value.expand(image_batch_tensor.sizes());

  auto image_batch_normalized_tensor = (image_batch_tensor - mean_value_broadcasted) / std_value_broadcasted;

//   Create a vector of inputs
//  std::vector<torch::jit::IValue> inputs;
//  inputs.push_back(torch::ones({1, 3, 224, 224}).to(at::kCUDA));

  // Execute the model and turn its output into a tensor.
  at::Tensor output = module->forward({image_batch_normalized_tensor}).toTensor();

  // After function call
  auto stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model (Resnet50 on ImageNet) processing time (" << device << "): " << static_cast<float>(duration.count()) / 1E6 << std::endl;


//  Tensor top_probability_indexes;
//  Tensor top_probabilies;

//  tie(top_probabilies, top_probability_indexes) = topk(softmaxed, 5, 1, true);

//  top_probability_indexes = top_probability_indexes.toBackend(Backend::CPU).view({-1});

//  auto accessor = top_probability_indexes.accessor<long,1>();

  auto max_result = output.max(1, true);
  auto max_index = std::get<1>(max_result).item<float>();
  std::cout << max_index << std::endl;

//  std::cout << output.slice(/*dim=*/1, /*start=*/0, /*end=*/100) << '\n';
//  std::cout << output << '\n';
}
