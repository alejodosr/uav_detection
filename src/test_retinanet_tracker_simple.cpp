#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

// Include retinanet model
#include <retinanet.h>

// Include ECO tracker
#include "eco/eco.hpp"
#include "eco/parameters.hpp"
#include "eco/metrics.hpp"
#include "eco/debug.hpp"

// Include colors detector
#include "colors_detector.h"

// Global variables (dont like this implementation)
cv::Point point1, point2; /* vertical points of the bounding box */
int drag = 0;
cv::Rect rect; /* bounding box */
cv::Mat img, roiImg; /* roiImg - the part of the image in the bounding box */
int select_flag = 0;

void mouseHandler(int event, int x, int y, int flags, void* param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
        /* left button clicked. ROI selection begins */
        point1 = cv::Point(x, y);
        drag = 1;
    }

    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
        /* mouse dragged. ROI being selected */
        cv::Mat img1 = img.clone();
        point2 = cv::Point(x, y);
        cv::rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        cv::imshow("image", img1);
    }

    if (event == CV_EVENT_LBUTTONUP && drag)
    {
        point2 = cv::Point(x, y);
        rect = cv::Rect(point1.x,point1.y,x-point1.x,y-point1.y);
        drag = 0;
    }

    if (event == CV_EVENT_LBUTTONUP)
    {
       /* ROI selected */
        select_flag = 1;
        drag = 0;
    }
}

int main(int argc, const char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_video> <device>\n";
    return -1;
  }

  // Drone class mapping
  const int DRONE_CLASS = 0;
  // Initial frame number
  const int FRAME_NO = 900;
  // Record video
  const string RECORD_VIDEO = "/media/alejandro/DATA/temporary/generated.avi";
  const float FPS = 25.0;
  // Minimum ecobox area to allow reinitialize the tracker
  const int MIN_ECO_BOX_AREA = 2300;
  // Minimum score to consider a proper UAV detection (while tracking)
  const float MIN_ON_SEARCH_SCORE = 0.7;
  // Period between detections (while tracking)
  const int PERIOD_DETECTIONS = 50;
  // Enable detect colors
  const int DETECT_COLORS = 0;
  // Manual initialization
  const int MANUAL_INITIALIZATION = 1;

  // Read video capture
  cv::VideoCapture cap(argv[2]);

  // Check if camera opened successfully
  if(!cap.isOpened()){
    std::cout << "Error opening video stream or file" << std::endl;
    return -1;
  }

  // Set initial frame
  cap.set(cv::CAP_PROP_POS_FRAMES, FRAME_NO);

  // Allocate retinanet
  std::shared_ptr<RetinanetModel> retinanet = std::make_shared<RetinanetModel>(argv[1], argv[3]);

  eco::ECO ecotracker;
  eco::EcoParameters parameters;

  parameters.useCnFeature = false;
  parameters.cn_features.fparams.tablename = "/usr/local/include/opentracker/eco/look_tables/CNnorm.txt";
  /* VOT2016_HC_settings
  parameters.useDeepFeature = false;
  parameters.useHogFeature = true;
  parameters.useColorspaceFeature = false;
  parameters.useCnFeature = true;
  parameters.useIcFeature = true;
  parameters.learning_rate = 0.01;
  parameters.projection_reg = 5e-7;
  parameters.init_CG_iter = 10 * 20;
  parameters.CG_forgetting_rate = 60;
  parameters.precond_reg_param = 0.2;
  parameters.reg_window_edge = 4e-3;
  parameters.use_scale_filter = false;
  */

  std::vector<float> found_bbs;
  float scale;
  std::tuple<std::vector<int>, std::vector<float>, std::vector<int>> results;
  std::vector<int> bbs;
  std::vector<float> scores;
  std::vector<int> classification;
  bool failure_detected = false;

  // Default resolution of the frame is obtained.The default resolution is system dependent.
  int frame_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
  int frame_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);

  cv::VideoWriter video;
  if (RECORD_VIDEO.length() > 0){
    // Define the codec and create VideoWriter object.The output is stored in 'outcpp.avi' file.
    video = cv::VideoWriter(RECORD_VIDEO, CV_FOURCC('M','J','P','G'), FPS, cv::Size(frame_width,frame_height));
  }

  cv::Rect selected_roi;

  if (MANUAL_INITIALIZATION == 0){
    // Infinite loop
    while(1){
        // Read image
        cv::Mat src;
        // Capture frame-by-frame
        cap >> src;

        // Clear
        found_bbs.clear();
        scores.clear();
        classification.clear();

        // If the frame is empty, break immediately
        if (src.empty())
          break;

        // Preprocess image
        std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
        float scale = std::get<0>(process_result);
        float offset_x = /*std::get<1>(process_result)*/ 0;
        float offset_y =/* std::get<2>(process_result)*/ 0;

        std::cout << "Found scale: " << scale << std::endl;
        std::cout << "Found offset_x: " << offset_x << std::endl;
        std::cout << "Found offset_y: " << offset_y << std::endl;
        std::cout << "Image boundaries (rows, cols): " << src.rows << "\t" << src.cols << std::endl;

        // Forward network
        results = retinanet->forward_(true);
        bbs = std::get<0>(results);
        scores = std::get<1>(results);
        classification = std::get<2>(results);

        // Look for drone class
        for(int i = 0; i<classification.size(); i++){
            if (classification[i] == DRONE_CLASS){
                found_bbs.push_back(float(bbs[i * 4]) * (1.0/scale)  + offset_x);
                found_bbs.push_back(float(bbs[i * 4 + 1]) * (1.0/scale) + offset_y);
                found_bbs.push_back(float(bbs[i * 4 + 2]) * (1.0/scale) + offset_x);
                found_bbs.push_back(float(bbs[i * 4 + 3]) * (1.0/scale) + offset_y);
                break;
            }
        }

        // Exit from global loop
        if(found_bbs.size() > 0)
            break;

        if (bbs.size() > 0){
            if (bbs[0] == -1){
                break;
            }
        }
    }
  }
  else{
    int k;
    cap >> img; /* get image(Mat) */
    cv::imshow("image", img);
    while(1)
    {
      cap >> img;
      cvSetMouseCallback("image", mouseHandler, NULL);
      if (select_flag == 1)
      {
//        cv::imshow("ROI", roiImg); /* show the image bounded by the box */
        break;
      }
      cv::rectangle(img, rect, CV_RGB(255, 0, 0), 3, 8, 0);
      cv::imshow("image", img);
      k = cv::waitKey(0);
      if (k == 27)
      {
        break;
      }
    }

    selected_roi = rect;
    found_bbs.push_back(float(selected_roi.x));
    found_bbs.push_back(float(selected_roi.y));
    found_bbs.push_back(float(selected_roi.x + selected_roi.width));
    found_bbs.push_back(float(selected_roi.y + selected_roi.height));
  }

  // If drone has been found
  if(found_bbs.size() > 0){
      // Initial bounding box
       cv::Rect2f ecobbox(found_bbs[0] /** (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0]) /** (1.0/scale)*/, (found_bbs[3] - found_bbs[1]) /** (1.0/scale)*/);

       // Read next frame
       cv::Mat src;

       // Capture frame-by-frame
       cap >> src;

       // Init tracker
       ecotracker.init(src, ecobbox, parameters);

       // Index
       int i = 0;

      // Infinite loop
      while(1){
          // Read image
          cv::Mat src;
          // Capture frame-by-frame
          cap >> src;

          // If the frame is empty, break immediately
          if (src.empty())
            break;

          // Copy to src draw
          cv::Mat src_draw;
          src.copyTo(src_draw);

          // Update ECO tracker
          bool okeco = ecotracker.update(src, ecobbox);

          // Detect colors
          cv::Rect color_rect;
          if (DETECT_COLORS){
            color_rect = DetectObjectInImage(src, "Red");
            if (color_rect.area() > 0){
              cv::rectangle(src_draw, color_rect, cv::Scalar(60, 60, 255), 2, 1);
            }
          }

          if (okeco && !failure_detected){
              cv::rectangle(src_draw, ecobbox, cv::Scalar(255, 60, 255), 2, 1); //blue
          }
          else{
              failure_detected = true;
              cv::putText(src_draw, "Target out of the image", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(255, 60, 255), 2);
          }

          //          std::cout << ecobbox.area() << std::endl;
          if (MANUAL_INITIALIZATION == 0){
            // Re-init tracker
            if ((i % PERIOD_DETECTIONS == 0 && (ecobbox.area() > MIN_ECO_BOX_AREA)) || (/*i % PERIOD_DETECTIONS == 0 &&*/ failure_detected)){
              std::cout << "Searching for new UAV detection" << std::endl;

              // Clear
              found_bbs.clear();
              scores.clear();
              classification.clear();

              // Preprocess image
              std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
              float scale = std::get<0>(process_result);
              float offset_x = std::get<1>(process_result);
              float offset_y = std::get<2>(process_result);

              std::cout << "Found scale: " << scale << std::endl;
              std::cout << "Found offset_x: " << offset_x << std::endl;
              std::cout << "Found offset_y: " << offset_y << std::endl;

              // Forward network
              results = retinanet->forward_(false);
              bbs = std::get<0>(results);
              scores = std::get<1>(results);
              classification = std::get<2>(results);

              for(int j = 0; j<classification.size(); j++){
                if (classification[j] == DRONE_CLASS && scores[j] > MIN_ON_SEARCH_SCORE){
                  found_bbs.push_back(float(bbs[j * 4]) * (1.0/scale)  + offset_x);
                  found_bbs.push_back(float(bbs[j * 4 + 1]) * (1.0/scale) + offset_y);
                  found_bbs.push_back(float(bbs[j * 4 + 2]) * (1.0/scale) + offset_x);
                  found_bbs.push_back(float(bbs[j * 4 + 3]) * (1.0/scale) + offset_y);
                }
              }

              // Exit from global loop
              if(found_bbs.size() > 0){
                std::cout << "ECO tracker has been re-initiated (" << i << ")" << std::endl;
                // Start time
                auto start = std::chrono::high_resolution_clock::now();
                cv::Rect2f ecobbox(found_bbs[0] /** (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0]) /** (1.0/scale)*/, (found_bbs[3] - found_bbs[1]) /** (1.0/scale)*/);
                ecotracker.init(src, ecobbox, parameters);
                // After function call
                auto stop = std::chrono::high_resolution_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
                // To get the value of duration use the count()t
                std::cout << "Time to re-init ECO tracker: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

                // Restore flag
                failure_detected = false;
              }
            }
          }

          // Show images
          cv::namedWindow( "Retinanet result", cv::WINDOW_AUTOSIZE );// Create a window for display.
//          cv::resize(src_draw, src_draw, cv::Size(src_draw.cols/2, src_draw.rows/2));
          cv::imshow( "Retinanet result", src_draw);
          char key = (char) cv::waitKey(3);   // explicit cast
          if (key == 27) break;                // break if `esc' key was pressed.
          if (key == 'f')   failure_detected = true;

          // Write frame
          if (RECORD_VIDEO.length() > 0){
            video.write(src_draw);
          }

          // Increase index
          i++;
      }
  }

  // When everything done, release the video capture object
  cap.release();

  if (RECORD_VIDEO.length() > 0){
    video.release();
  }

  // Closes all the frames
  cv::destroyAllWindows();
}
