 #include <iostream>
#include <memory>
#include <random>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

// Opencv trackers
#include <opencv2/tracking.hpp>

//adding ros class
#include "ros_image_handler.h"

// Include detector
#include "detector.h"

// Include pose estimator
#include "pose_estimator.h"

// Colors for terminal
#include "colors.h"

// Colors
Color::Modifier red(Color::FG_RED);
Color::Modifier blue(Color::FG_BLUE);
Color::Modifier green(Color::FG_GREEN);
Color::Modifier def(Color::FG_DEFAULT);

// Show time function
void printFrequency(std::vector<std::tuple<std::string, float>> &infos){
  std::tuple<std::string, float> info;

  // Print detector
  std::cout << red << std::get<0>(infos[0]) << ": " << std::get<1>(infos[0]);
  // Print tracker
  std::cout << "\t" << blue << std::get<0>(infos[1]) << ": " << std::get<1>(infos[1]);
  // Print pose estimator
  std::cout << "\t" << green << std::get<0>(infos[2]) << ": " << std::get<1>(infos[2]) << def << std::endl;

}

int main(int argc, char **argv) {
  //  if (argc != 4) {
  //    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
  //    return -1;
  //  }

  // Classes of detector
  const int DRONE=0, BALL=1;

  // Ball mode (It enables averaging sides of the bounding box detections and selects the proper class)
  bool BALL_MODE = true;

  // Minimum ecobox area to allow reinitialize the tracker
  const int MAX_IN_A_ROW_FAILURES = 5;

  // Period between detections (while tracking)
  const int PERIOD_DETECTIONS = 40;

  // Confidence of the detector to update tracker
  const float CONFIDENCE_THRS = 0.1;

  // Size of the object to be detected
  //double OBJECT_WIDTH = 0.30;		// Bebop 2
  //double OBJECT_HEIGHT = 0.25;
  //double OBJECT_ELEVATION = 0.10;
  double OBJECT_WIDTH = 0.14;		// Ball
  double OBJECT_HEIGHT = 0.14;
  double OBJECT_ELEVATION = 0.14;
 
  // Tracker selection
  const std::string tracker_name = "CSRT"; // CSRT, MOSSE


  // Local variables
  auto start = std::chrono::high_resolution_clock::now();
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::tuple<std::string, float> info;
  std::vector<std::tuple<std::string, float>> infos;
  info = std::make_tuple("Detector", -1.0);
  infos.push_back(info);
  info = std::make_tuple("Tracker", -1.0);
  infos.push_back(info);
  info = std::make_tuple("Pose Est.", -1.0);
  infos.push_back(info);

  std::vector<cv::Point2d> image_points;
  float  confidence = 0.0f, x, y, w, h;


  // Info
  std::cout << "YOLO DETECTOR and TRACKING" << std::endl;

  //allocate ros image handler
  std::string model_path, device;
  int show_image;
  ros_image_handle image_handler;

  image_handler.init_node(argc, argv, "tracker_ros");
  image_handler.read_params(model_path, device, show_image);

  if(model_path.empty() || device.empty())
  {
    std::cout << "model path or device not set! " << std::endl;
    return -1;
  }

  // Init detector
  YOLO_detector detector(model_path);

  // opencv tracker
  cv::Ptr<cv::Tracker> tracker;
	if ((tracker_name.compare("CSRT")) == 0){
		tracker = cv::TrackerCSRT::create();
	}  		
	else if ((tracker_name.compare("MOSSE")) == 0){
		tracker = cv::TrackerMOSSE::create();
	}
	else{
		std::cout << "ERROR: tracker not found" << std::endl;
		return -1;
	}

  std::vector<int> found_bbs, classification_output;
  std::vector<float>  scores_output;
  bool failure_detected = false;

  // Get image
  cv::Mat src;
  image_handler.get_cv_image(src);

  // Info
  std::cout << "Waiting to get first image..." << std::endl;

  // Wait to get first image
  while(image_handler.spin()){
    image_handler.get_cv_image(src);

    if(src.cols > 0)
      break;
  }

  // Info
  std::cout << "OK: receiving images" << std::endl;

  // Wait for first detection
  auto detection = detector.detect(src);
  confidence = 0.0f;
  detection.clear();

  while((detection.size() == 0 || confidence < CONFIDENCE_THRS)){
    // Ros spin
    image_handler.spin();

    // Get opencv image
    image_handler.get_cv_image(src);

    // Time measurement
    start = std::chrono::high_resolution_clock::now();

    // Detect
    detection = detector.detect(src);
    for(auto it = detection.begin(); it != detection.end(); ++it){
	std::cout << "Class name: " << (*it).c << std::endl;
	std::cout << "confidence: " << (*it).confidence << std::endl;
	int det_class = (*it).c;
	if (detection.size() > 0 && ((det_class && BALL_MODE) || (!det_class && !BALL_MODE))){
	   confidence = (*it).confidence;
	   x = (*it).x;
	   y = (*it).y;
	   w = (*it).w;
	   h = (*it).h;
	   if (confidence > CONFIDENCE_THRS)
		break;
	}
	else
	   confidence = 0.0f;
    }

    // After function call
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    info = std::make_tuple("Detector", 1.0 / (static_cast<float>(duration.count()) / 1E6));
    infos[0] = info;

    // Show image
    if(show_image == 1)
      image_handler.publish_detected_image(src, std::vector<int>(), std::vector<float>(), std::vector<int>());
  }

  // If detection has been found
  if(detection.size() > 0){
    if (BALL_MODE){
    	w = (w + h) / 2;
	h = w;
    }
	
    // Initial bounding box
    cv::Rect2f bbox(x, y, w, h);

    // Read next frame
    cv::Mat src;
    image_handler.get_cv_image(src);

    // Define camera parameters (Bebop 2)
    //double camera_params[9] = { 536.774231, 0.000000, 432.798910, 0.000000, 527.838517, 222.208294, 0.000000, 0.000000, 1.000000 };
    //cv::Mat CAMERA_PARAMETERS = cv::Mat(3, 3, CV_64F, camera_params);
    //double distortion_coeff[5] = { 0.002459, 0.005719, -0.010226, 0.004865, 0.000000 };
    //cv::Mat DIST_COEFFS = cv::Mat(1, 5, CV_64F, distortion_coeff);

    // Define camera parameters (M210)
    double camera_params[9] = { 914.355693, 0.000000, 638.950226, 0.000000, 911.255370, 392.747341, 0.000000, 0.000000, 1.000000 };
    cv::Mat CAMERA_PARAMETERS = cv::Mat(3, 3, CV_64F, camera_params);
    double distortion_coeff[5] = { 0.010869, 0.006590, 0.003729, -0.002629, 0.000000 };
    cv::Mat DIST_COEFFS = cv::Mat(1, 5, CV_64F, distortion_coeff);


    // Instance pose estimator
    PoseEstimator pose_estimator(CAMERA_PARAMETERS, DIST_COEFFS);

    // Set 3D object points
    std::vector<cv::Point3d> object_points; // OBJECT points (object frame of reference in the center of the ball)
    //// 1---------2 ////
    //// |         | ////
    //// |         | ////
    //// 4---------3 ////
    object_points.push_back(cv::Point3d(- OBJECT_WIDTH / 2, - OBJECT_ELEVATION / 2, 0.0)); // 1
    object_points.push_back(cv::Point3d(+ OBJECT_WIDTH / 2, - OBJECT_ELEVATION / 2, 0.0)); // 2
    object_points.push_back(cv::Point3d(+ OBJECT_WIDTH / 2, + OBJECT_ELEVATION / 2, 0.0)); // 3
    object_points.push_back(cv::Point3d(- OBJECT_WIDTH / 2, + OBJECT_ELEVATION / 2, 0.0)); // 4

//    object_points.push_back(cv::Point3d(- OBJECT_WIDTH / 2, - OBJECT_HEIGHT / 2, 0.0)); // 1
//    object_points.push_back(cv::Point3d(+ OBJECT_WIDTH / 2, - OBJECT_HEIGHT / 2, 0.0)); // 2
//    object_points.push_back(cv::Point3d(+ OBJECT_WIDTH / 2, + OBJECT_HEIGHT / 2, 0.0)); // 3
//    object_points.push_back(cv::Point3d(- OBJECT_WIDTH / 2, + OBJECT_HEIGHT / 2, 0.0)); // 4
    pose_estimator.set3DObjectPoints(object_points);

    // Detect
//    auto detection = detector.detect(src);

    // Time measurement
    start = std::chrono::high_resolution_clock::now();
    // Init tracker
    tracker->init(src, bbox);
    // After function call
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    info = std::make_tuple("Tracker", 1.0 / (static_cast<float>(duration.count()) / 1E6));
    infos[1] = info;

    // Index
    int i = 1, failures_no = 0;

    // Infinite loop
    while(1){
      if(image_handler.spin()){
        // Read image
        cv::Mat src;
        image_handler.get_cv_image(src);

        // If the frame is empty, break immediately
        if (src.empty())
          continue;

        // Copy to src draw
        cv::Mat src_draw;
        src.copyTo(src_draw);

        // Time measurement
        start = std::chrono::high_resolution_clock::now();

        // Update tracker
        cv::Rect2d local;
        if (i == 1)
          local = bbox;
        bool ok = tracker->update(src, local);

        // After function call
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        info = std::make_tuple("Tracker", 1.0 / (static_cast<float>(duration.count()) / 1E6));
        infos[1] = info;
        printFrequency(infos);

        if (ok && !failure_detected){
          failures_no = 0;

          cv::rectangle(src_draw, local, cv::Scalar(255, 0, 255), 2, 1); //blue
          std::vector<int> tracker_bbs, tracker_classification;
          std::vector<float> tracker_scores;

          // Fill vectors
          tracker_bbs.push_back(local.x);
          tracker_bbs.push_back(bbox.y);
          tracker_bbs.push_back(local.x + local.width);
          tracker_bbs.push_back(local.y + local.height);
          tracker_classification.push_back(0);
          tracker_scores.push_back(1.0);

          // Send first bounding box
          image_handler.publish_bb(tracker_bbs, tracker_scores, tracker_classification);
          if(show_image == 1)
            image_handler.publish_detected_image(src_draw, tracker_bbs, tracker_scores, tracker_classification);

          // Time measurement
          start = std::chrono::high_resolution_clock::now();

          // Calculate pose of the object
          image_points.clear();
          image_points.push_back(cv::Point2d(local.x, local.y));
          image_points.push_back(cv::Point2d(local.x + local.width, local.y));
          image_points.push_back(cv::Point2d(local.x + local.width, local.y + local.height));
          image_points.push_back(cv::Point2d(local.x, local.y + local.height));
          pose_estimator.set2DImagePoints(image_points);
          cv::Mat rot, trans, rot_rod;
          pose_estimator.estimatePose(rot, trans, 0x3);
          //          cv::Rodrigues(rot, rot_rod);

          // Publish tf of the pose of the object
          std::vector<double> rot_vector = {rot.at<double>(0), rot.at<double>(1), rot.at<double>(2)};
          std::vector<double> trans_vector = {trans.at<double>(0), trans.at<double>(1), trans.at<double>(2)};
          image_handler.publish_tf(rot_vector, trans_vector);

          // After function call
          stop = std::chrono::high_resolution_clock::now();
          duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
          info = std::make_tuple("Pose Est.", 1.0 / (static_cast<float>(duration.count()) / 1E6));
          infos[2] = info;
          printFrequency(infos);

        }
        else{
          failures_no++;
          if (failures_no > MAX_IN_A_ROW_FAILURES){
            failure_detected = true;
            cv::putText(src_draw, "Tracking failure detected", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(255, 0, 255), 2);
            std::cout << red << "ERROR: Tracker failed" << def << std::endl;
          }
        }

        // Exit from global loop
        if(image_handler.get_reset() || (i % PERIOD_DETECTIONS == 0) || failure_detected){

          std::cout << "Tracker has been re-initiated (" << i << ")" << std::endl;

          // Detect
          detection.clear();
          confidence = 0.0f;

          while((detection.size() == 0 || confidence < CONFIDENCE_THRS) /*|| (detection.back().w * detection.back().h < MIN_ECO_BOX_AREA)*/){
            // Ros spin
            image_handler.spin();

            // Get opencv image
            image_handler.get_cv_image(src);
            src.copyTo(src_draw);

            // Time measurement
            start = std::chrono::high_resolution_clock::now();

            // Update tracker
            tracker->update(src, local);

            // After function call
            stop = std::chrono::high_resolution_clock::now();
            duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            info = std::make_tuple("Tracker", 1.0 / (static_cast<float>(duration.count()) / 1E6));
            infos[1] = info;
            printFrequency(infos);

            // Time measurement
            start = std::chrono::high_resolution_clock::now();

            // Detect
            detection = detector.detect(src);
	    for(auto it = detection.begin(); it != detection.end(); ++it){
		std::cout << "Class name: " << (*it).c << std::endl;
		std::cout << "confidence: " << (*it).confidence << std::endl;
		int det_class = (*it).c;
		if (detection.size() > 0 && ((det_class && BALL_MODE) || (!det_class && !BALL_MODE))){
		   confidence = (*it).confidence;
		   x = (*it).x;
		   y = (*it).y;
		   w = (*it).w;
		   h = (*it).h;
		   if (confidence > CONFIDENCE_THRS)
			break;
		}
		else
		   confidence = 0.0f;
	    }


            // After function call
            stop = std::chrono::high_resolution_clock::now();
            duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            info = std::make_tuple("Detector", 1.0 / (static_cast<float>(duration.count()) / 1E6));
            infos[0] = info;
            printFrequency(infos);

//            cv::namedWindow( "Detector result", cv::WINDOW_AUTOSIZE );// Create a window for display.
//            cv::imshow( "Detector result", src);
//            char key = (char) cv::waitKey(1);   // explicit cast
//            if (key == 27)    break;                // break if `esc' key was pressed.
//            if (key == 'f')   failure_detected = true;

            cv::rectangle(src_draw, local, cv::Scalar(255, 0, 255), 2, 1); //blue

            // Show image
            if(show_image == 1)
              image_handler.publish_detected_image(src_draw, std::vector<int>(), std::vector<float>(), std::vector<int>());

            if (!failure_detected) // Quick detector check, if not cancel
              break;
          }

          if (detection.size() > 0 && confidence > CONFIDENCE_THRS /*&& (detection.back().w * detection.back().h > MIN_ECO_BOX_AREA)*/){
              // Debug
              std::cout << "Detected RoI confidence: " << confidence << std::endl;

              try{
                // Time measurement
                start = std::chrono::high_resolution_clock::now();

	        if (BALL_MODE){
	    	   w = (w + h) / 2;
		   h = w;
	        }

                cv::Rect2f bbox(x, y, w, h);
                local = bbox;
  		if (tracker_name.compare("CSRT") == 0){
                	tracker = cv::TrackerCSRT::create();
		}  		
		else if (tracker_name.compare("MOSSE") == 0){
                	tracker = cv::TrackerMOSSE::create();
		}
		else{
			std::cout << "ERROR: tracker not found" << std::endl;
			return -1;
		}
                bool ok_init = tracker->init(src, local);
                if (!ok_init){
                  std::cout << red << "ERROR: Tracker re-init failed" << def << std::endl;
                }

                // After function call
                stop = std::chrono::high_resolution_clock::now();
                duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
                info = std::make_tuple("Tracker Init", 1.0 / (static_cast<float>(duration.count()) / 1E6));
                infos[1] = info;
                printFrequency(infos);
              }
              catch(...){
                std::cout << red << "EXCEPTION: Tracker failed to init" << std::endl;
              }
          }
          else{
            std::cout << "Detected RoI NOT GOOD ENOUGH to update tracker: " << confidence << std::endl;
          }

          // Restore flag
          failure_detected = false;
        }

        // Show images
//        cv::namedWindow( "Detector result", cv::WINDOW_AUTOSIZE );// Create a window for display.
//        cv::imshow( "Detector result", src_draw);
//        char key = (char) cv::waitKey(1);   // explicit cast
//        if (key == 27)    break;                // break if `esc' key was pressed.
//        if (key == 'f')   failure_detected = true;

        // Increase index
        i++;
      }
    }
  }
}
