 #include <iostream>
#include <memory>
#include <random>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

// Include ECO tracker
#include "eco/eco.hpp"
#include "eco/parameters.hpp"
#include "eco/metrics.hpp"
#include "eco/debug.hpp"

//adding ros class
#include "ros_image_handler.h"

// Include detector
#include "detector.h"

// Include pose estimator
#include "pose_estimator.h"

int main(int argc, char **argv) {
  //  if (argc != 4) {
  //    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
  //    return -1;
  //  }

  // Minimum ecobox area to allow reinitialize the tracker
  const int MIN_ECO_BOX_AREA = 2300;
  // Period between detections (while tracking)
  const int PERIOD_DETECTIONS = 40;
  // Confidence of the detector to update tracker
  const float CONFIDENCE_THRS = 0.3;

  // Info
  std::cout << "YOLO DETECTOR and TRACKING" << std::endl;

  //allocate ros image handler
  std::string model_path, device;
  int show_image;
  ros_image_handle image_handler;

  image_handler.init_node(argc, argv, "tracker_ros");
  image_handler.read_params(model_path, device, show_image);

  if(model_path.empty() || device.empty())
  {
    std::cout << "model path or device not set! " << std::endl;
    return -1;
  }

  // Init detector
  YOLO_detector detector(model_path);

  // ECO tracker
  static eco::ECO ecotracker;
  static eco::EcoParameters parameters;

  parameters.useCnFeature = false;
//   parameters.cn_features.fparams.tablename = "/home/alejandro/workspace/uav_detection/src/uav_detection/include/eco/look_tables/CNnorm.txt";
  parameters.cn_features.fparams.tablename = "/home/cvar/workspace/uav_detection/src/uav_detection/include/eco/look_tables/CNnorm.txt";
  /* VOT2016_HC_settings   */
  parameters.useDeepFeature = false;
  parameters.useHogFeature = true;
  parameters.useColorspaceFeature = false;
  parameters.useCnFeature = false;
  parameters.useIcFeature = false;
  parameters.learning_rate = 0.01;
  parameters.projection_reg = 5e-7;
  parameters.init_CG_iter = 10 * 20;
  parameters.CG_forgetting_rate = 60;
  parameters.precond_reg_param = 0.2;
  parameters.reg_window_edge = 4e-3;
  parameters.use_scale_filter = false;


  std::vector<int> found_bbs, classification_output;
  std::vector<float>  scores_output;
  bool failure_detected = false;

  // Get image
  cv::Mat src;
  image_handler.get_cv_image(src);

  // Info
  std::cout << "Waiting to get first image..." << std::endl;

  // Wait to get first image
  while(image_handler.spin()){
    image_handler.get_cv_image(src);

    if(src.cols > 0)
      break;
  }

  // Info
  std::cout << "OK: receiving images" << std::endl;

  // Wait for first detection
  auto detection = detector.detect(src);
  // Debug
//  if (detection.size() > 0){
//        std::cout << "First detected RoI: " << detection.back().x << "\t" << detection.back().y << "\t" << detection.back().w << "\t" << detection.back().h << std::endl;
//        cv::rectangle(src, cv::Rect(detection.back().x, detection.back().y, detection.back().w, detection.back().h), cv::Scalar(255, 0, 255), 2, 1); //blue
//        cv::imshow( "Detector result", src);
//        cv::waitKey(1);
//  }
  while(detection.size() == 0){
    // Ros spin
    image_handler.spin();

    // Get opencv image
    image_handler.get_cv_image(src);

    // Detect
    detection = detector.detect(src);

    // Debug
//    if (detection.size() > 0){
//      std::cout << "First detected RoI: " << detection.back().x << "\t" << detection.back().y << "\t" << detection.back().w << "\t" << detection.back().h << std::endl;
//      cv::rectangle(src, cv::Rect(detection.back().x, detection.back().y, detection.back().w, detection.back().h), cv::Scalar(255, 0, 255), 2, 1); //blue
//      cv::imshow( "Detector result", src);
//      cv::waitKey(1);
//    }

    // Show image
    if(show_image == 1)
      image_handler.publish_detected_image(src, std::vector<int>(), std::vector<float>(), std::vector<int>());
  }

  // If drone has been found
  if(detection.size() > 0){
    // Initial bounding box
    cv::Rect2f ecobbox(detection.back().x, detection.back().y, detection.back().w, detection.back().h);

    // Read next frame
    cv::Mat src;
    image_handler.get_cv_image(src);

    // Define camera parameters (Bebop 2)
    //double camera_params[9] = { 536.774231, 0.000000, 432.798910, 0.000000, 527.838517, 222.208294, 0.000000, 0.000000, 1.000000 };
    //cv::Mat CAMERA_PARAMETERS = cv::Mat(3, 3, CV_64F, camera_params);
    //double distortion_coeff[5] = { 0.002459, 0.005719, -0.010226, 0.004865, 0.000000 };
    //cv::Mat DIST_COEFFS = cv::Mat(1, 5, CV_64F, distortion_coeff);

    // Define camera parameters (Bebop 2)
    double camera_params[9] = { 914.355693, 0.000000, 638.950226, 0.000000, 911.255370, 392.747341, 0.000000, 0.000000, 1.000000 };
    cv::Mat CAMERA_PARAMETERS = cv::Mat(3, 3, CV_64F, camera_params);
    double distortion_coeff[5] = { 0.010869, 0.006590, 0.003729, -0.002629, 0.000000 };
    cv::Mat DIST_COEFFS = cv::Mat(1, 5, CV_64F, distortion_coeff);


    // Instance pose estimator
    PoseEstimator pose_estimator(CAMERA_PARAMETERS, DIST_COEFFS);

    // Set 3D object points
    std::vector<cv::Point3d> object_points; // BALL points (object frame of reference in the center of the ball)
    double BALL_DIAMETER = 0.25; // 25 cm
    //// 1-----2 ////
    //// |     | ////
    //// |     | ////
    //// 4-----3 ////
    object_points.push_back(cv::Point3d(- BALL_DIAMETER / 2, - BALL_DIAMETER / 2, 0.0)); // 1
    object_points.push_back(cv::Point3d(+ BALL_DIAMETER / 2, - BALL_DIAMETER / 2, 0.0)); // 2
    object_points.push_back(cv::Point3d(+ BALL_DIAMETER / 2, + BALL_DIAMETER / 2, 0.0)); // 3
    object_points.push_back(cv::Point3d(- BALL_DIAMETER / 2, + BALL_DIAMETER / 2, 0.0)); // 4
    pose_estimator.set3DObjectPoints(object_points);

    // Detect
    auto detection = detector.detect(src);

    auto start = std::chrono::high_resolution_clock::now();
    // Init tracker
    ecotracker.init(src, ecobbox, parameters);
    // After function call
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    // To get the value of duration use the count()t
    std::cout << "FIRST init ECO tracker: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

    // Index
    int i = 1;

    // Infinite loop
    while(1){
      if(image_handler.spin()){
        // Read image
        cv::Mat src;
        image_handler.get_cv_image(src);

        // If the frame is empty, break immediately
        if (src.empty())
          continue;

        // Copy to src draw
        cv::Mat src_draw;
        src.copyTo(src_draw);

        // Update ECO tracker
        bool okeco = ecotracker.update(src, ecobbox);

        if (okeco && !failure_detected){
          cv::rectangle(src_draw, ecobbox, cv::Scalar(255, 0, 255), 2, 1); //blue
          std::vector<int> tracker_bbs, tracker_classification;
          std::vector<float> tracker_scores;

          // Fill vectors
          tracker_bbs.push_back(ecobbox.x);
          tracker_bbs.push_back(ecobbox.y);
          tracker_bbs.push_back(ecobbox.x + ecobbox.width);
          tracker_bbs.push_back(ecobbox.y + ecobbox.height);
          tracker_classification.push_back(0);
          tracker_scores.push_back(1.0);

          // Send first bounding box
          image_handler.publish_bb(tracker_bbs, tracker_scores, tracker_classification);
          if(show_image == 1)
            image_handler.publish_detected_image(src_draw, tracker_bbs, tracker_scores, tracker_classification);

          // Calculate pose of the object
          std::vector<cv::Point2d> image_points;
          image_points.push_back(cv::Point2d(ecobbox.x, ecobbox.y));
          image_points.push_back(cv::Point2d(ecobbox.x + ecobbox.width, ecobbox.y));
          image_points.push_back(cv::Point2d(ecobbox.x + ecobbox.width, ecobbox.y + ecobbox.height));
          image_points.push_back(cv::Point2d(ecobbox.x, ecobbox.y + ecobbox.height));
          pose_estimator.set2DImagePoints(image_points);
          cv::Mat rot, trans, rot_rod;
          pose_estimator.estimatePose(rot, trans, 0x2);
//          cv::Rodrigues(rot, rot_rod);

          // Publish tf of the pose of the object
          std::vector<double> rot_vector = {rot.at<double>(0), rot.at<double>(1), rot.at<double>(2)};
          std::vector<double> trans_vector = {trans.at<double>(0), trans.at<double>(1), trans.at<double>(2)};
          image_handler.publish_tf(rot_vector, trans_vector);
        }
        else{
          failure_detected = true;
          cv::putText(src_draw, "ECO tracking failure detected", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(255, 0, 255), 2);
        }

        // Exit from global loop
        if(image_handler.get_reset() || (i % PERIOD_DETECTIONS == 0 && (ecobbox.area() > MIN_ECO_BOX_AREA)) || failure_detected){
          std::cout << "ECO tracker has been re-initiated (" << i << ")" << std::endl;

          // Detect
          auto detection = detector.detect(src);
          float  confidence = 0.0f;

          while(detection.size() == 0 || confidence < CONFIDENCE_THRS || (detection.back().w * detection.back().h < MIN_ECO_BOX_AREA)){
            // Ros spin
            image_handler.spin();

            // Get opencv image
            image_handler.get_cv_image(src);

            // Update ECO tracker
            ecotracker.update(src, ecobbox);

            // Detect
            auto start = std::chrono::high_resolution_clock::now();
            detection = detector.detect(src);
            if (detection.size() > 0)
              confidence = detection.back().confidence;
            else
              confidence = 0.0f;
            // After function call
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            // To get the value of duration use the count()t
            std::cout << "TIME: Time of detector inference: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

            // Update ECO tracker
            ecotracker.update(src, ecobbox);

//            cv::namedWindow( "Detector result", cv::WINDOW_AUTOSIZE );// Create a window for display.
//            cv::imshow( "Detector result", src);
//            char key = (char) cv::waitKey(1);   // explicit cast
//            if (key == 27)    break;                // break if `esc' key was pressed.
//            if (key == 'f')   failure_detected = true;

            // Show image
            if(show_image == 1)
              image_handler.publish_detected_image(src, std::vector<int>(), std::vector<float>(), std::vector<int>());

            if (!failure_detected) // Quick detector check, if not cancel
              break;
          }

          if (detection.size() > 0 && confidence > CONFIDENCE_THRS && (detection.back().w * detection.back().h > MIN_ECO_BOX_AREA)){
              // Debug
              std::cout << "Detected RoI confidence: " << detection.back().confidence << std::endl;

              // Start time
              auto start = std::chrono::high_resolution_clock::now();
              cv::Rect2f ecobbox(detection.back().x, detection.back().y, detection.back().w, detection.back().h);
              ecotracker.init(src, ecobbox, parameters, true);
              // After function call
              auto stop = std::chrono::high_resolution_clock::now();
              auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
              // To get the value of duration use the count()t
              std::cout << "TIME: Time to re-init ECO tracker: " << static_cast<float>(duration.count()) / 1E6 << std::endl;
          }
          else{
            std::cout << "Detected RoI NOT GOOD ENOUGH to update tracker: " << confidence << std::endl;
          }

          // Restore flag
          failure_detected = false;
        }

        // Show images
//        cv::namedWindow( "Detector result", cv::WINDOW_AUTOSIZE );// Create a window for display.
//        cv::imshow( "Detector result", src_draw);
//        char key = (char) cv::waitKey(1);   // explicit cast
//        if (key == 27)    break;                // break if `esc' key was pressed.
//        if (key == 'f')   failure_detected = true;

        // Increase index
        i++;
      }
    }
  }
}
