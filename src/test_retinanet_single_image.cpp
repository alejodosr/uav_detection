#include <torch/script.h> // One-stop header.
#include "anchors.h"
#include "bbox_transform.h"
#include "clip_boxes.h"
#include "utils.h"

#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

void showImage(cv::Mat image)
{
    cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );// Create a window for display.
    cv::imshow("Display window", image );
    cv::waitKey(0);
}

void resizeAndPad(cv::Mat &src, cv::Mat &dst, int min_side = 512, int max_side = 512){
    // Copy image
    cv::Mat tmp;
    src.copyTo(tmp);

    // Check scale
    int shortest_side = std::min(tmp.rows, tmp.cols);

    // Compute scale
    float scale = float(min_side) / float(shortest_side);

    if(scale > 0.0){
        // Compute largest side
        int largest_side = std::max(tmp.rows, tmp.cols);
        if (float(largest_side) * scale > float(max_side)){
            scale = float(max_side) / float(largest_side);
        }

        std::cout << "Scale of the transformation: " << scale << std::endl;

        cv::resize(tmp, tmp, cv::Size(), scale, scale);
    }

    // Pad image if required
    int pad_w = max_side - tmp.rows%max_side;
    int pad_h = max_side - tmp.cols%max_side;

    if (tmp.rows%max_side == 0){
        pad_w = 0;
    }
    if (tmp.cols%max_side == 0){
        pad_h = 0;
    }

    dst = cv::Mat::zeros(cv::Size(tmp.rows + pad_w, tmp.cols + pad_h), CV_8UC3);

    cv::Rect roi(0,0, tmp.cols, tmp.rows);

    // Copy to padded image
    tmp.copyTo(dst(roi));
}

int main(int argc, const char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
    return -1;
  }

  // Define the size of the input square tensor
  const int SQUARE_SIZE = 512;
  // Define number of classes
  const int NUM_CLASSES = 3;
  // Define the threshold to be considered from the class
  const float THRESHOLD = 0.5;

  // Deserialize the ScriptModule from a file using torch::jit::load().
  std::shared_ptr<torch::jit::script::Module> module = torch::jit::load(argv[1]);
  const std::string image_path = argv[2];
  const std::string device = argv[3];

  assert(module != nullptr);
  std::cout << "[OK] Module deserialized correctly\n";

  if (device == "gpu"){
      auto start = std::chrono::high_resolution_clock::now();
      //  Move to CUDA
      module->to(at::kCUDA);
      // After function call
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      // To get the value of duration use the count()
      // member function on the duration object
      std::cout << "Time to move module to GPU memory: " << static_cast<float>(duration.count()) / 1E6 << std::endl;
  }

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  auto start = std::chrono::high_resolution_clock::now();

  // Read image
  cv::Mat src;
  src = cv::imread(image_path, CV_LOAD_IMAGE_COLOR);
  cv::cvtColor(src, src, cv::COLOR_BGR2RGB);
  cv::Mat image;
//  cv::resize(src, image, cv::Size(SQUARE_SIZE,SQUARE_SIZE));
  resizeAndPad(src, image, SQUARE_SIZE, SQUARE_SIZE);
//  showImage(image);

  // From image to tensor
  std::vector<int64_t> sizes = {image.rows, image.cols, 3};
  at::TensorOptions options(at::ScalarType::Byte);
  at::Tensor output_tensor = torch::from_blob(image.data, at::IntList(sizes), options);

  auto image_tensor = output_tensor.toType(at::kFloat) / 255;
  if (device == "gpu")
    image_tensor = image_tensor.to(at::kCUDA);

  // Reshape image into 1 x 3 x height x width
  auto image_batch_tensor = image_tensor.transpose(0, 2).transpose(1, 2).unsqueeze(0);

  // Normalize batch
  auto mean_value = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
  if (device == "gpu")
      mean_value = mean_value.to(at::kCUDA);

  mean_value[0][0][0][0] = 0.485f;
  mean_value[0][1][0][0] = 0.456f;
  mean_value[0][2][0][0] = 0.406f;

  // Broadcast the value
  auto mean_value_broadcasted = mean_value.expand(image_batch_tensor.sizes());

  auto std_value = torch::ones({1, 3, 1, 1}).toType(at::kFloat);
  if (device == "gpu")
      std_value = std_value.to(at::kCUDA);

  std_value[0][0][0][0] = 0.229f;
  std_value[0][1][0][0] = 0.224f;
  std_value[0][2][0][0] = 0.225f;

  auto std_value_broadcasted = std_value.expand(image_batch_tensor.sizes());

  auto image_batch_normalized_tensor = (image_batch_tensor - mean_value_broadcasted) / std_value_broadcasted;
  auto image_batch_normalized_tensor_cpu = image_batch_normalized_tensor.to(at::kCPU);

//   Create a vector of inputs
//  std::vector<torch::jit::IValue> inputs;
//  inputs.push_back(torch::ones({1, 3, 224, 224}).to(at::kCUDA));

  // Execute the model and turn its output into a tensor.
  at::Tensor output = module->forward({image_batch_normalized_tensor}).toTensor();

  // After function call
  auto stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model Retinanet processing time (" << device << "): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Get regression and classifications
  auto regression = output.slice(2, 0, 4);
  auto classification = output.slice(2, 4, 5 + NUM_CLASSES);

  // Generate anchors
  auto anchors_module = std::make_shared<Anchors>();
  anchors_module->to(at::kCPU);

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model Anchors allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Inference on anchors model
  at::Tensor anchors = anchors_module->forward({image_batch_normalized_tensor_cpu});

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model Anchors processing time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Generate transforms
  auto bbox_module = std::make_shared<BBoxTransform>();

  bbox_module->to(at::kCPU);
  anchors = anchors.to(at::kCPU);
  regression = regression.to(at::kCPU);

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model Bbox allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Inference on anchors model
  at::Tensor transformed_anchors = bbox_module->forward({anchors}, {regression});

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model Bbox processing time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Generate transforms
  auto clip_boxes_module = std::make_shared<ClipBoxes>();
  // Move model to CPU
  clip_boxes_module->to(at::kCPU);
  transformed_anchors = transformed_anchors.to(at::kCPU);

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model ClipBoxes allocation time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  // Inference on anchors model
  transformed_anchors = clip_boxes_module->forward({transformed_anchors}, {image_batch_normalized_tensor_cpu});

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "Model ClipBoxes processing time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  // Use auto keyword to avoid typing long
  // type definitions to get the timepoint
  // at this instant use function now()
  start = std::chrono::high_resolution_clock::now();

  auto scores_tuple = torch::max(classification, 2, true);
  auto scores = std::get<0>(scores_tuple);
  auto indices = std::get<1>(scores_tuple);

  auto keep_count = non_max_supression(transformed_anchors.squeeze(0).detach().cpu(), scores.squeeze(0).detach().cpu());

  auto keep = std::get<0>(keep_count);
  auto count = std::get<1>(keep_count);

  // After function call
  stop = std::chrono::high_resolution_clock::now();

  // Subtract stop and start timepoints and
  // cast it to required unit. Predefined units
  // are nanoseconds, microseconds, milliseconds,
  // seconds, minutes, hours. Use duration_cast()
  // function.
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

  // To get the value of duration use the count()
  // member function on the duration object
  std::cout << "NMS processing time (cpu): " << static_cast<float>(duration.count()) / 1E6 << std::endl;

  cv::cvtColor(image, image, cv::COLOR_RGB2BGR);

  for(int i = 0; i < count; i++){
    int index = keep.slice(0, i, i +1).squeeze(1).item<int>();
    if(scores.slice(1, index, index + 1).squeeze(0).squeeze(1).item<float>() > THRESHOLD){
        int x1 = transformed_anchors.slice(1, index, index + 1).slice(2, 0, 1).squeeze(0).item<int>();
        int y1 = transformed_anchors.slice(1, index, index + 1).slice(2, 1, 2).squeeze(0).item<int>();
        int x2 = transformed_anchors.slice(1, index, index + 1).slice(2, 2, 3).squeeze(0).item<int>();
        int y2 = transformed_anchors.slice(1, index, index + 1).slice(2, 3, 4).squeeze(0).item<int>();
        cv::rectangle(image, cv::Point(x1, y1), cv::Point(x2, y2), cv::Scalar(0, 255, 0), 2);
    }
  }

  cv::namedWindow( "Retinanet result", cv::WINDOW_AUTOSIZE );// Create a window for display.
  cv::imshow( "Retinanet result", image );
  cv::waitKey(0);
}
