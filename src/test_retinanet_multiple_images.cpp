#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

#include <retinanet.h>

int main(int argc, const char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
    return -1;
  }

  // Init counter
  int i = 0;

  // Allocate retinanet
  std::shared_ptr<RetinanetModel> retinanet = std::make_shared<RetinanetModel>(argv[1], argv[3]);

  // Infinite loop
  while(1){
      // Increase counter
      i++;

      // Read image
      cv::Mat src;
      src = cv::imread(argv[2] + std::to_string(i) + ".jpg", CV_LOAD_IMAGE_COLOR);
      if (src.empty()){
        src = cv::imread(argv[2] + std::to_string(i) + ".png", CV_LOAD_IMAGE_COLOR);
        if (src.empty()){
            break;
         }
      }

      // Preprocess image
      std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
      float scale = std::get<0>(process_result);
      float offset_x = std::get<1>(process_result);
      float offset_y = std::get<2>(process_result);

      // Forward network
      std::tuple<std::vector<int>, std::vector<float>, std::vector<int>> results = retinanet->forward_(true);
      std::vector<int> bbs = std::get<0>(results);
      std::vector<float> scores = std::get<1>(results);
      if (bbs.size() > 0){
          if (bbs[0] == -1){
              break;
          }
      }
  }
}
