#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

// Include retinanet model
#include <retinanet.h>

// Include ECO tracker
#include "eco/eco.hpp"
#include "eco/parameters.hpp"
#include "eco/metrics.hpp"
#include "eco/debug.hpp"

//adding ros class
#include "ros_image_handler.h"

int main(int argc, char **argv) {
  //  if (argc != 4) {
  //    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
  //    return -1;
  //  }

  // Drone class mapping
  const int DRONE_CLASS = 0;
  // Initial frame number
  //  const int FRAME_NO = 100;
  // Minimum ecobox area to allow reinitialize the tracker
  const int MIN_ECO_BOX_AREA = 2300;
  // Minimum score to consider a proper UAV detection (while tracking)
  const float MIN_ON_SEARCH_SCORE = 0.5;
  // Period between detections (while tracking)
  const int PERIOD_DETECTIONS = 40;

  //allocate ros image handler
  std::string model_path, device;
  int show_image;
  ros_image_handle image_handler;

  image_handler.init_node(argc, argv, "retinanet_ros");
  image_handler.read_params(model_path, device, show_image);

  if(model_path.empty() || device.empty())
  {
    std::cout << "model path or device not set! " << std::endl;
    return -1;
  }

  // Allocate retinanet
  std::shared_ptr<RetinanetModel> retinanet = std::make_shared<RetinanetModel>(model_path, device);

  eco::ECO ecotracker;
  eco::EcoParameters parameters;

  parameters.useCnFeature = false;
  parameters.cn_features.fparams.tablename = "/usr/local/include/opentracker/eco/look_tables/CNnorm.txt";
  /* VOT2016_HC_settings
  parameters.useDeepFeature = false;
  parameters.useHogFeature = true;
  parameters.useColorspaceFeature = false;
  parameters.useCnFeature = true;
  parameters.useIcFeature = true;
  parameters.learning_rate = 0.01;
  parameters.projection_reg = 5e-7;
  parameters.init_CG_iter = 10 * 20;
  parameters.CG_forgetting_rate = 60;
  parameters.precond_reg_param = 0.2;
  parameters.reg_window_edge = 4e-3;
  parameters.use_scale_filter = false;
  */

  std::vector<int> found_bbs, classification_output;
  float scale;
  std::tuple<std::vector<int>, std::vector<float>, std::vector<int>> results;
  std::vector<int> bbs;
  std::vector<float> scores, scores_output;
  std::vector<int> classification;
  bool failure_detected = false;
  cv::Mat src_output;

  // Infinite loop
  while(1){
    //spining the node inorder to messages
    if(image_handler.spin()){
      //get the image from ros
      cv::Mat src;
      image_handler.get_cv_image(src);

      // Clear
      found_bbs.clear();
      scores.clear();
      classification.clear();

      // If the frame is empty, continue and try again
      if (src.empty())
        continue;

      // Preprocess image
      std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
      float scale = std::get<0>(process_result);
      float offset_x = std::get<1>(process_result);
      float offset_y = std::get<2>(process_result);

      std::cout << "Found scale: " << scale << std::endl;
      std::cout << "Found offset_x: " << offset_x << std::endl;
      std::cout << "Found offset_y: " << offset_y << std::endl;

      // Forward network
      results = retinanet->forward_(true);
      bbs = std::get<0>(results);
      scores = std::get<1>(results);
      classification = std::get<2>(results);

      scores_output.clear();
      classification_output.clear();

      // Look for drone class
      for(int i = 0; i < classification.size(); i++){
        if (classification[i] == DRONE_CLASS){
          found_bbs.push_back(float(bbs[i * 4]) * (1.0/scale)  + offset_x);
          found_bbs.push_back(float(bbs[i * 4 + 1]) * (1.0/scale) + offset_y);
          found_bbs.push_back(float(bbs[i * 4 + 2]) * (1.0/scale) + offset_x);
          found_bbs.push_back(float(bbs[i * 4 + 3]) * (1.0/scale) + offset_y);
          scores_output.push_back(scores[i]);
          classification_output.push_back(int(classification[i]));
          src.copyTo(src_output);
          break;
        }
      }

      // Send first bounding box
      image_handler.publish_bb(found_bbs, scores_output, classification_output);
      if(show_image == 1)
        image_handler.publish_detected_image(src_output, found_bbs, scores_output, classification_output);

      // Exit from global loop
      if(found_bbs.size() > 0)
        break;

      if (bbs.size() > 0){
        if (bbs[0] == -1){
          break;
        }
      }
    }
  }

  // If drone has been found
  if(found_bbs.size() > 0){
    // Initial bounding box
    cv::Rect2f ecobbox(found_bbs[0] /** (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0]) /** (1.0/scale)*/, (found_bbs[3] - found_bbs[1]) /** (1.0/scale)*/);

    // Read next frame
    cv::Mat src;
    image_handler.get_cv_image(src);

    // Init tracker
    ecotracker.init(src, ecobbox, parameters);

    // Index
    int i = 1;

    // Infinite loop
    while(1){
      if(image_handler.spin()){
        // Read image
        cv::Mat src;
        image_handler.get_cv_image(src);

        // If the frame is empty, break immediately
        if (src.empty())
          continue;

        // Copy to src draw
        cv::Mat src_draw;
        src.copyTo(src_draw);

        // Update ECO tracker
        bool okeco = ecotracker.update(src, ecobbox);

        if (okeco && !failure_detected){
          cv::rectangle(src_draw, ecobbox, cv::Scalar(255, 0, 255), 2, 1); //blue
          std::vector<int> tracker_bbs, tracker_classification;
          std::vector<float> tracker_scores;

          // Fill vectors
          tracker_bbs.push_back(ecobbox.x);
          tracker_bbs.push_back(ecobbox.y);
          tracker_bbs.push_back(ecobbox.x + ecobbox.width);
          tracker_bbs.push_back(ecobbox.y + ecobbox.height);
          tracker_classification.push_back(0);
          tracker_scores.push_back(1.0);

          // Send first bounding box
          image_handler.publish_bb(tracker_bbs, tracker_scores, tracker_classification);
          if(show_image == 1)
            image_handler.publish_detected_image(src, tracker_bbs, tracker_scores, tracker_classification);
        }
        else{
          failure_detected = true;
          cv::putText(src_draw, "Tracking failure detected", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(255, 60, 255), 2);
        }

        //          std::cout << ecobbox.area() << std::endl;

        // Re-init tracker
        if ((i % PERIOD_DETECTIONS == 0 && (ecobbox.area() > MIN_ECO_BOX_AREA)) || (i % PERIOD_DETECTIONS == 0 && failure_detected)){
          std::cout << "Searching for new UAV detection" << std::endl;

          // Clear
          found_bbs.clear();
          scores.clear();
          classification.clear();

          // Preprocess image
          std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
          float scale = std::get<0>(process_result);
          float offset_x = std::get<1>(process_result);
          float offset_y = std::get<2>(process_result);

          // Forward network
          results = retinanet->forward_(false);
          bbs = std::get<0>(results);
          scores = std::get<1>(results);
          classification = std::get<2>(results);

          for(int j = 0; j<classification.size(); j++){
            if (classification[j] == DRONE_CLASS && scores[j] > MIN_ON_SEARCH_SCORE){
              found_bbs.push_back(float(bbs[j * 4]) * (1.0/scale)  + offset_x);
              found_bbs.push_back(float(bbs[j * 4 + 1]) * (1.0/scale) + offset_y);
              found_bbs.push_back(float(bbs[j * 4 + 2]) * (1.0/scale) + offset_x);
              found_bbs.push_back(float(bbs[j * 4 + 3]) * (1.0/scale) + offset_y);
            }
          }

          // Exit from global loop
          if(found_bbs.size() > 0){
            std::cout << "ECO tracker has been re-initiated (" << i << ")" << std::endl;
            // Start time
            auto start = std::chrono::high_resolution_clock::now();
            cv::Rect2f ecobbox(found_bbs[0]/* * (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0])/* * (1.0/scale)*/, (found_bbs[3] - found_bbs[1])/* * (1.0/scale)*/);
            ecotracker.init(src, ecobbox, parameters);
            // After function call
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            // To get the value of duration use the count()t
            std::cout << "Time to re-init ECO tracker: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

            // Restore flag
            failure_detected = false;
          }
        }

//        // Show images
                  cv::namedWindow( "Retinanet result", cv::WINDOW_AUTOSIZE );// Create a window for display.
                  cv::imshow( "Retinanet result", src_draw);
                  char key = (char) cv::waitKey(1);   // explicit cast
                  if (key == 27) break;                // break if `esc' key was pressed.
                  if (key == 'f')   failure_detected = true;

        // Increase index
        i++;
      }
    }
  }
}
