 #include <iostream>
#include <memory>
#include <random>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

// Include ECO tracker
#include "eco/eco.hpp"
#include "eco/parameters.hpp"
#include "eco/metrics.hpp"
#include "eco/debug.hpp"

//adding ros class
#include "ros_image_handler.h"

int main(int argc, char **argv) {
  //  if (argc != 4) {
  //    std::cerr << "usage: example-app <path-to-exported-script-module> <path_to_image> <device>\n";
  //    return -1;
  //  }

  // Minimum ecobox area to allow reinitialize the tracker
  const int MIN_ECO_BOX_AREA = 2300;
  // Size of one size of the bounding box
  const int SIDE_BB_SIZE = 100;

  //allocate ros image handler
  std::string model_path, device;
  int show_image;
  ros_image_handle image_handler;

  image_handler.init_node(argc, argv, "tracker_ros");
  image_handler.read_params(model_path, device, show_image);

  if(model_path.empty() || device.empty())
  {
    std::cout << "model path or device not set! " << std::endl;
    return -1;
  }

  eco::ECO ecotracker;
  eco::EcoParameters parameters;

  parameters.useCnFeature = false;
  parameters.cn_features.fparams.tablename = "/usr/local/include/opentracker/eco/look_tables/CNnorm.txt";
  /* VOT2016_HC_settings
  parameters.useDeepFeature = false;
  parameters.useHogFeature = true;
  parameters.useColorspaceFeature = false;
  parameters.useCnFeature = true;
  parameters.useIcFeature = true;
  parameters.learning_rate = 0.01;
  parameters.projection_reg = 5e-7;
  parameters.init_CG_iter = 10 * 20;
  parameters.CG_forgetting_rate = 60;
  parameters.precond_reg_param = 0.2;
  parameters.reg_window_edge = 4e-3;
  parameters.use_scale_filter = false;
  */

  std::vector<int> found_bbs, classification_output;
  std::vector<float>  scores_output;
  bool failure_detected = false;

  // Get image
  cv::Mat src;
  image_handler.get_cv_image(src);

  // Wait to get first image
  while(image_handler.spin()){
    image_handler.get_cv_image(src);

    if(src.cols > 0)
      break;
  }

  // Generate random tracker
  // Init the random number generator
  std::random_device rd; // obtain a random number from hardware
  std::mt19937 eng(rd()); // seed the generator
  std::uniform_int_distribution<> distr_width(SIDE_BB_SIZE + 1, src.cols - SIDE_BB_SIZE - 1); // define the range
  std::uniform_int_distribution<> distr_height(SIDE_BB_SIZE + 1, src.rows - SIDE_BB_SIZE - 1); // define the range

  // Reset bounding box
  found_bbs.clear();
  scores_output.clear();
  classification_output.clear();
  int x1 = distr_width(eng);
  int y1 = distr_height(eng);
  found_bbs.push_back(x1);
  found_bbs.push_back(y1);
  found_bbs.push_back(x1 + SIDE_BB_SIZE);
  found_bbs.push_back(y1 + SIDE_BB_SIZE);
  scores_output.push_back(1.0);
  classification_output.push_back(0.0);

  // If drone has been found
  if(found_bbs.size() > 0){
    // Initial bounding box
    cv::Rect2f ecobbox(found_bbs[0] /** (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0]) /** (1.0/scale)*/, (found_bbs[3] - found_bbs[1]) /** (1.0/scale)*/);

    // Read next frame
    cv::Mat src;
    image_handler.get_cv_image(src);

    // Init tracker
    ecotracker.init(src, ecobbox, parameters);

    // Index
    int i = 1;

    // Infinite loop
    while(1){
      if(image_handler.spin()){
        // Read image
        cv::Mat src;
        image_handler.get_cv_image(src);

        // If the frame is empty, break immediately
        if (src.empty())
          continue;

        // Copy to src draw
        cv::Mat src_draw;
        src.copyTo(src_draw);

        // Update ECO tracker
        bool okeco = ecotracker.update(src, ecobbox);

        if (okeco && !failure_detected){
          cv::rectangle(src_draw, ecobbox, cv::Scalar(255, 0, 255), 2, 1); //blue
          std::vector<int> tracker_bbs, tracker_classification;
          std::vector<float> tracker_scores;

          // Fill vectors
          tracker_bbs.push_back(ecobbox.x);
          tracker_bbs.push_back(ecobbox.y);
          tracker_bbs.push_back(ecobbox.x + ecobbox.width);
          tracker_bbs.push_back(ecobbox.y + ecobbox.height);
          tracker_classification.push_back(0);
          tracker_scores.push_back(1.0);

          // Send first bounding box
          image_handler.publish_bb(tracker_bbs, tracker_scores, tracker_classification);
          if(show_image == 1)
            image_handler.publish_detected_image(src, tracker_bbs, tracker_scores, tracker_classification);
        }
        else{
          failure_detected = true;
          cv::putText(src_draw, "ECO tracking failure detected", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(255, 0, 255), 2);
        }

        //          std::cout << ecobbox.area() << std::endl;

        // Exit from global loop
        if(image_handler.get_reset()){
          std::cout << "ECO tracker has been re-initiated (" << i << ")" << std::endl;

          // Generate random tracker
          // Init the random number generator
          std::random_device rd; // obtain a random number from hardware
          std::mt19937 eng(rd()); // seed the generator
          std::uniform_int_distribution<> distr_width(SIDE_BB_SIZE + 1, src.cols - SIDE_BB_SIZE - 1); // define the range
          std::uniform_int_distribution<> distr_height(SIDE_BB_SIZE + 1, src.rows - SIDE_BB_SIZE - 1); // define the range

          // Reset bounding box
          found_bbs.clear();
          scores_output.clear();
          classification_output.clear();
          int x1 = distr_width(eng);
          int y1 = distr_height(eng);
          found_bbs.push_back(x1);
          found_bbs.push_back(y1);
          found_bbs.push_back(x1 + SIDE_BB_SIZE);
          found_bbs.push_back(y1 + SIDE_BB_SIZE);
          scores_output.push_back(1.0);
          classification_output.push_back(0.0);

          // Start time
          auto start = std::chrono::high_resolution_clock::now();
          cv::Rect2f ecobbox(found_bbs[0] /** (1.0/scale)*/, found_bbs[1] /** (1.0/scale)*/, (found_bbs[2] - found_bbs[0]) /** (1.0/scale)*/, (found_bbs[3] - found_bbs[1]) /** (1.0/scale)*/);
          ecotracker.init(src, ecobbox, parameters);
          // After function call
          auto stop = std::chrono::high_resolution_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
          // To get the value of duration use the count()t
          std::cout << "Time to re-init ECO tracker: " << static_cast<float>(duration.count()) / 1E6 << std::endl;

          // Restore flag
          failure_detected = false;
        }

        // Show images
        cv::namedWindow( "Retinanet result", cv::WINDOW_AUTOSIZE );// Create a window for display.
        cv::imshow( "Retinanet result", src_draw);
        char key = (char) cv::waitKey(1);   // explicit cast
        if (key == 27) break;                // break if `esc' key was pressed.
        if (key == 'f')   failure_detected = true;

        // Increase index
        i++;
      }
    }
  }
}
