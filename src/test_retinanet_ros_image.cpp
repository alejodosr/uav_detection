#include <iostream>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>

#include <retinanet.h>

//adding ros class
#include "ros_image_handler.h"


int main(int argc, char **argv)
{
    //allocate ros image handler
    std::string model_path, device;
    int show_image;
    ros_image_handle image_handler;

    image_handler.init_node(argc, argv, "retinanet_ros");
    image_handler.read_params(model_path, device, show_image);

    if(model_path.empty() || device.empty())
    {
        std::cout << "model path or device not set! " << std::endl;
        return -1;
    }

    // Allocate retinanet
    std::shared_ptr<RetinanetModel> retinanet = std::make_shared<RetinanetModel>(model_path, device);

    // Infinite loop
    while(1){

        //spining the node inorder to messages
        if(image_handler.spin()){
            //get the image from ros
            cv::Mat src;
            image_handler.get_cv_image(src);

            if(!src.empty())
            {
                // Preprocess image
              std::tuple<float, float, float> process_result = retinanet->preprocess_image_(src);
              float scale = std::get<0>(process_result);
              float offset_x = std::get<1>(process_result);
              float offset_y = std::get<2>(process_result);

                // Forward network
                std::tuple<std::vector<int>, std::vector<float>, std::vector<int>> results = retinanet->forward_(false);
                std::vector<int> bbs = std::get<0>(results);
                std::vector<float> scores = std::get<1>(results);
                std::vector<int> classes = std::get<2>(results);

                std::vector<int> bbs_output;
                std::vector<float> scores_output;
                std::vector<int> classes_output;

                //publishing the bbs
                if(bbs.size() > 0)
                {

                    //rescaling the bb
                    for(int i =0; i < bbs.size(); i++)
                    {
                        bbs[i] = bbs[i];
                        if (i % 4 == 0){
                            if (scores[i / 4] > 0.7){
                                bbs_output.push_back(float(bbs[i]) * (1.0/scale)  + offset_x);
                                bbs_output.push_back(float(bbs[i + 1]) * (1.0/scale) + offset_y);
                                bbs_output.push_back(float(bbs[i + 2]) * (1.0/scale) + offset_x);
                                bbs_output.push_back(float(bbs[i + 3]) * (1.0/scale) + offset_y);
                                scores_output.push_back(scores[i / 4]);
                                classes_output.push_back(classes[i / 4]);
                            }
                        }
                    }

                }

                image_handler.publish_bb(bbs_output, scores_output, classes_output);
                if(show_image == 1)
                    image_handler.publish_detected_image(src, bbs_output, scores_output, classes_output);

            }
        }

    }



}

